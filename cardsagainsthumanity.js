// Description:
//   Cards against humanity, in Hubot form. Talk to Hubot in a private channel
//   and he'll deal out cards to you and anyone else that wants to play.
//
// Commands:
//   hubot cah play - Join the game
//   hubot cah new round - Force to restart the round with a new black card
//   hubot cah hand - See the white cards you have to use
//   hubot cah submit [# # # ...] - Submit your answer(s)
//   hubot cah answers - (For judges only) See the list of answers people have given
//   hubot cah choose # - (For judges only) Choose the best answer
//   hubot cah broadcast [...] - Broadcast a message to players, only if you're a player
//   hubot cah new hand - This will replace your entire hand at the cost of a point
//   hubot cah retire - Leave the game

var game = require('./game').create(),
    async = require('async'),
    util = require('util');

var blankRegex = /_/g;
var replacement = '____';

function noop () {}

function sender (msg) {
  return msg.message.user.name.toLowerCase();
}

function prettyBlackCard (card) {
  return '> ' + card.text.replace(blankRegex, replacement) + ' [' + card.pick + ']';
}

function prettyHand (cards) {
  return cards.map(function (card, index) {
    return '> ' + index + ' : ' + card;
  }).join('\n');
}

function anonPrintSubmissions (blackCard, subs) {
  return subs.map(function (sub, index) {
    return '> ' + index + ' : ' + insertAnswersIntoBlackCard(blackCard, sub.answers);
  }).join('\n');
}

function prettyPrintSubmissions (blackCard, winningIndex, subs, scores) {
  var winner = subs[winningIndex];
  subs.splice(winningIndex, 1);

  var output = '';
  output += '*** Winning answer by ' + winner.name + ' *** \n';
  output += '> ' + insertAnswersIntoBlackCard(blackCard, winner.answers) + '\n';
  output += '\n';
  output += 'Others: \n';
  output += subs.map(function (sub) {
    return sub.name + ' : ' + insertAnswersIntoBlackCard(blackCard, sub.answers);
  }).join('\n');
  output += '\n';
  output += 'Scores:\n';
  output += scores.map(function (score) {
    return score.name + ' : ' + score.score;
  }).join('\n');
  output += '\n';

  return output;
}

function insertAnswersIntoBlackCard (blackCard, answers) {
  var response = blackCard.text,
      blanks = blackCard.text.match(blankRegex);

  if (!blanks || blanks.length != blackCard.pick) return blackCard.text.replace(blankRegex, replacement) + ' [' + answers + ']';

  answers.forEach(function (answer) {
    response = response.replace(/_/, '*' + answer + '*');
  });

  return response;
}

function broadcastToPlayers (robot, message, callback) {
  async.eachSeries(game.getPlayers(), function (player, next) {
    if (game.playerIsAI(player)) return next();
    robot.messageRoom(player, message);
    setTimeout(next, 500);
  }, callback);
}

function sendPlayersTheirHands (robot, callback) {
  async.eachSeries(game.getPlayers(), function (player, next) {
    if (game.playerIsAI(player)) return next();
    robot.messageRoom(player,
      '\n' +
      'Your hand (use "cah submit # [# # ...]"):' +
      '\n' +
      prettyHand(game.getPlayerHand(player))
    );

    setTimeout(next, 500);
  }, callback);
}

module.exports = function (robot) {
  robot.respond(/cah play$/, function (msg) {
    var name = sender(msg);
    game.addPlayer(name, function (err, players) {
      if (err) return robot.messageRoom(name, 'You\'re already playing.');
    });
  });

  game.on('player-added', function (info) {
    broadcastToPlayers(robot,
      info.name + ' has joined the game.' + '\n' +
      'The game now has ' + info.players.length +
                        (info.players.length > 1 ? ' players.' : ' player.') +
                        'Those playing are: ' + info.players + '\n' +
      'When ready to start the game, type > cah new round',
    noop);
  });

  robot.respond(/cah new game/, function (msg) {
    var name = sender(msg);
    if (game.getPlayers().indexOf(name) == -1) return;

    broadcastToPlayers(robot, 'Game Over', function () {
      game.reset();
    });
  });

  robot.respond(/cah submit (([0-4]\s*)*)\s*$/, function (msg) {
    var name = sender(msg);
    if (game.getPlayers().indexOf(name) == -1) return;
    if (!game.getBlackCard()) return robot.messageRoom(name, 'Not started yet...');
    if (game.getPlayers().length > 1 && name == game.getJudge()) return robot.messageRoom(name, 'You\'re the judge - what are you doing!?');

    var submissionString = msg.match[1],
        submissions = submissionString.split(/\s/),
        unique = submissions.filter(function(item, i, ar){ return ar.indexOf(item) === i; });

    if (unique.length != game.getBlackCard().pick)
      return robot.messageRoom(name, 'Need ' + game.getBlackCard().pick + (game.getBlackCard().pick > 1 ? ' answers' : ' answer') + ', you gave ' + unique.length);

    game.submitAnswer(name, unique, function (err) {
      if (err) return robot.messageRoom(name, err);

      robot.messageRoom(name, 'Thank you for submitting.');
    });
  });

  game.on('submission', function (subCount) {
    if (this.state.aiPlayers.indexOf(this.getJudge()) > -1) return;

    robot.messageRoom(game.getJudge(),
      'Submission entered. Now at ' + subCount + '/' + (game.getPlayers().length -1) + '\n' +
      'Card: ' + prettyBlackCard(game.getBlackCard()) + '\n' +
      'Answers (When ready, use "cah choose #"):' + '\n' +
      anonPrintSubmissions(game.getBlackCard(), game.getSubmissions()));
  });

  robot.respond(/cah answers/, function (msg) {
    var name = sender(msg);
    if (game.getPlayers().indexOf(name) == -1) return;

    if (name != game.getJudge()) return robot.messageRoom(name, 'You are not the judge, bugger off you thieving gypsy!');

    robot.messageRoom(game.getJudge(),
      'Card: ' + prettyBlackCard(game.getBlackCard()) + '\n' +
      'Answers (When ready, use "cah choose #"):' + '\n' +
      anonPrintSubmissions(game.getBlackCard(), game.getSubmissions())
    );
  });

  robot.respond(/cah choose ([0-9]+)\s*/, function (msg) {
    var name = sender(msg);
    if (game.getPlayers().indexOf(name) == -1) return;

    if (name != game.getJudge()) return robot.messageRoom(name, 'You are not the judge, bugger off you thieving gypsy!');

    var selection = +msg.match[1];
    if (selection > game.getSubmissions().length) return robot.messageRoom(game.getJudge(), 'Invalid choice.');

    game.judgeChooses(selection);
  });

  game.on('judge-choice', function (info) {
    broadcastToPlayers(robot, prettyPrintSubmissions(game.getBlackCard(), info.selection, info.submissions, game.getScores()), function () {
      game.newRound();
    });
  });

  robot.respond(/cah new round/, function (msg) {
    var name = sender(msg);
    if (game.getPlayers().indexOf(name) == -1) return;

    game.newRound();
  });

  game.on('new-round', function () {
    var judge = game.getJudge();

    broadcastToPlayers(robot,
      'New Round starting:' + '\n' +
      'Judge: ' + game.getJudge() + '\n' +
      prettyBlackCard(game.getBlackCard()),
      function () {
        sendPlayersTheirHands(robot);
    });
  });

  robot.respond(/cah hand/, function (msg) {
    var name = sender(msg);
    if (game.getPlayers().indexOf(name) == -1) return;

    robot.messageRoom(name, prettyHand(game.getPlayerHand(name)));
  });

  robot.respond(/cah broadcast (.*)$/, function (msg) {
    var name = sender(msg);
    if (game.getPlayers().indexOf(name) == -1) return;

    broadcastToPlayers(robot, 'Broadcast from [' + name + ']: ' +msg.match[1], noop);
  });

  robot.respond(/cah new hand/, function (msg) {
    var name = sender(msg);
    if (game.getPlayers().indexOf(name) == -1) return;

    game.newHandFor(name);
    robot.messageRoom(name, prettyHand(game.getPlayerHand(name)));
  });

  robot.respond(/cah retire/, function (msg) {
    var name = sender(msg);
    if (game.getPlayers().indexOf(name) == -1) return;
    if (game.getJudge() == name) return robot.messageRoom(name, 'You can\'t leave yet, you\'re the judge.');

    broadcastToPlayers(robot, '> ' + name + ' has retired.', noop);

    game.removePlayer(name);
  });

  robot.respond(/cah add ai/, function (msg) {
    var name = sender(msg);
    if (game.getPlayers().indexOf(name) == -1) return;

    game.addAI();
  });

  robot.respond(/cah debug/, function (msg) {
    var name = sender(msg);
    if (game.getPlayers().indexOf(name) == -1) return;

    robot.messageRoom(name, util.inspect(game.debug()));
  });
};
