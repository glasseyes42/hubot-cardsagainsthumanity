var cards = require('./cards/bundle');

var blankRegex = /_/g;

console.info('It is expected a couple might fail - for example haiku ones.');

cards.blackCards.forEach(function (card) {
  var matches = card.text.match(blankRegex),
      count = 1;
  if (matches) count = matches.length;

  if (count != card.pick) {
    console.error('Looky here!', card);
  }
});
