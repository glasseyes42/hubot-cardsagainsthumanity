var deck = require('./cards/bundle'),
    EventEmitter = require('events').EventEmitter,
    util = require('util');

function Game () {
  EventEmitter.call(this);

  this.state = {};
  this.reset();

  this.on('new-round', this.runAiPlayers);

  this.on('submission', this.runAiJudge);
}
util.inherits(Game, EventEmitter);

Game.prototype.reset = function () {
  this.state = {
    players: [],
    blackCard: null,
    hands: {},
    answers: [],
    judge: null,
    submissions: [],
    scores: [],
    usedWhiteCardIndicies: [],
    usedBlackCardIndicies: [],
    aiCounter: 0,
    aiPlayers: []
  };
};

Game.prototype.addPlayer = function (playerName, callback) {
  if (this.state.players.indexOf(playerName) == -1) {
    this.state.players.push(playerName);
    this.emit('player-added', { name: playerName, players: this.state.players});
    return callback(null, this.state.players);
  } else {
    return callback(true);
  }
};



Game.prototype.removePlayer = function (playerName) {
  var judge = this.getJudge();
  this.state.players.splice(this.state.players.indexOf(playerName), 1);
  this.state.judge = this.state.players.indexOf(judge);
};

Game.prototype.getPlayerHand = function (player) {
  if (!this.state.hands[player]) return [];
  return this.state.hands[player].map(function (index) {
    return deck.whiteCards[index];
  });
};

Game.prototype.getBlackCard = function () {
  return deck.blackCards[this.state.blackCard];
};

Game.prototype.getPlayers = function () {
  return this.state.players;
};

Game.prototype.getJudge = function () {
  return this.state.players[this.state.judge];
};

Game.prototype.getSubmissions = function () {
  return this.state.submissions;
};

Game.prototype.getScores = function () {
  return this.state.scores.sort(function (a,b) { return a.score - b.score; }).reverse();
};

Game.prototype.newRound = function () {
  this.state.submissions = [];
  this.pickJudge();
  this.selectBlackCard();
  this.buildHands();
  this.emit('new-round');
};

Game.prototype.pickJudge = function () {
  if (typeof this.state.judge === 'undefined') {
    this.state.judge = Math.floor(Math.random() * this.state.players.length);
    return;
  }
  this.state.judge = (this.state.judge + 1) % this.state.players.length;
};

Game.prototype.selectBlackCard = function () {
  if (this.state.usedBlackCardIndicies.length == deck.blackCards.length) return false;

  var index = Math.floor(Math.random() * deck.blackCards.length);

  while (this.state.usedBlackCardIndicies.indexOf(index) != -1) {
    index = Math.floor(Math.random() * deck.blackCards.length);
  }

  this.state.blackCard = index;
  this.state.usedBlackCardIndicies.push(index);
  return deck.blackCards[index];
};

Game.prototype.buildHands = function () {
  this.state.players.forEach(this.buildHandFor.bind(this));
};

Game.prototype.buildHandFor = function (player) {
  if (!this.state.hands[player]) this.state.hands[player] = [];
  if (this.state.hands[player].length == 5) return;

  while (this.state.hands[player].length < 5 &&
          this.state.usedWhiteCardIndicies.length != deck.whiteCards.length) {

    var whiteIndex = Math.floor(Math.random() * deck.whiteCards.length);

    if (this.state.usedWhiteCardIndicies.indexOf(whiteIndex) == -1) {
      this.state.hands[player].push(whiteIndex);
      this.state.usedWhiteCardIndicies.push(whiteIndex);
    }
  }
};

Game.prototype.newHandFor = function (player) {
  var playerIndex = null;
  for (var i in this.state.scores) {
    if (this.state.scores[i].name == player) playerIndex = i;
  }
  if (!this.state.hands[player]) return;
  if (playerIndex) this.state.scores[playerIndex].score--;
  else this.state.scores.push({name: player, score: -1});

  this.state.hands[player].splice(0, this.state.hands[player].length);
  this.buildHandFor(player);
};

Game.prototype.submitAnswer = function (player, submissionsIndicies, callback) {
  if (this.state.submissions.filter(function (sub) { return sub.name == player; }).length > 0) return callback('You already submitted');

  this.state.submissions.push({
    name: player,
    answers: submissionsIndicies.map(function(index) { return deck.whiteCards[this.state.hands[player][index]]; }.bind(this))
  });

  submissionsIndicies.sort().reverse().forEach(function (index) {
    this.state.hands[player].splice(index, 1);
  }.bind(this));

  this.emit('submission', this.state.submissions.length);

  return callback(null);
};

Game.prototype.judgeChooses = function (index) {
  var winner = this.state.submissions[index];

  var winnerIndex = null;
  for (var i in this.state.scores) {
    if (this.state.scores[i].name == winner.name) winnerIndex = i;
  }

  if (winnerIndex) this.state.scores[winnerIndex].score++;
  else this.state.scores.push({name: winner.name, score: 1});

  this.emit('judge-choice', {
    selection: index,
    submissions: this.state.submissions
  });
};

Game.prototype.addAI = function () {
  var name = 'AI_player' + (++this.state.aiCounter);

  this.addPlayer(name, function (){});
  this.state.aiPlayers.push(name);

};

Game.prototype.runAiPlayers = function () {
  this.state.aiPlayers.forEach(function (aiPlayerName) {
    if (this.getJudge() == aiPlayerName) return;

    var submissions = [],
        handIndicies = [0,1,2,3,4];

    for (var i=0; i < this.getBlackCard().pick; i++) {
      submissions.push(handIndicies.splice(Math.floor(Math.random() * handIndicies.length), 1));
    }

    setTimeout(function () {
      this.submitAnswer(aiPlayerName, submissions, function (){});
    }.bind(this), Math.random() * 5000);

  }.bind(this));
};

Game.prototype.runAiJudge = function () {
  if (this.state.aiPlayers.indexOf(this.getJudge()) < 0) return;

  if (this.state.submissions.length == this.state.players.length - 1) {
    var winner = Math.floor(Math.random() * this.state.submissions.length);

    setTimeout(function () {
      this.judgeChooses(winner);
    }.bind(this), Math.random() * 10000);

  }
};

Game.prototype.playerIsAI = function (name) {
  return this.state.aiPlayers.indexOf(name) > -1;
};

Game.prototype.debug = function () {
  return this.state;
};

module.exports = {
  create: function () {
    return new Game();
  }
};
