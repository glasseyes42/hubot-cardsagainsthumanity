module.exports = {
  "blackCards": [{
    "text": "Why can't I sleep at night?",
    "pick": 1
  }, {
    "text": "I got 99 problems but _ ain't one.",
    "pick": 1
  }, {
    "text": "What's a girl's best friend?",
    "pick": 1
  }, {
    "text": "What's that smell?",
    "pick": 1
  }, {
    "text": "This is the way the world ends \/ This is the way the world ends \/ Not with a bang but with _.",
    "pick": 1
  }, {
    "text": "What is Batman's guilty pleasure?",
    "pick": 1
  }, {
    "text": "TSA guidelines now prohibit _ on airplanes.",
    "pick": 1
  }, {
    "text": "What ended my last relationship?",
    "pick": 1
  }, {
    "text": "MTV's new reality show features eight washed-up celebrities living with _.",
    "pick": 1
  }, {
    "text": "I drink to forget _.",
    "pick": 1
  }, {
    "text": "I'm sorry, Professor, but I couldn't complete my homework because of _.",
    "pick": 1
  }, {
    "text": "Alternative medicine is now embracing the curative powers of _.",
    "pick": 1
  }, {
    "text": "What's that sound?",
    "pick": 1
  }, {
    "text": "What's the next Happy Meal&reg; toy?",
    "pick": 1
  }, {
    "text": "It's a pity that kids these days are all getting involved with _.",
    "pick": 1
  }, {
    "text": "In the new Disney Channel Original Movie, Hannah Montana struggles with _ for the first time.",
    "pick": 1
  }, {
    "text": "_. That's how I want to die.",
    "pick": 1
  }, {
    "text": "What does Dick Cheney prefer?",
    "pick": 1
  }, {
    "text": "What's the most emo?",
    "pick": 1
  }, {
    "text": "Instead of coal, Santa now gives the bad children _.",
    "pick": 1
  }, {
    "text": "Next from J.K. Rowling: Harry Potter and the Chamber of _.",
    "pick": 1
  }, {
    "text": "A romantic, candlelit dinner would be incomplete without _.",
    "pick": 1
  }, {
    "text": "White people like _.",
    "pick": 1
  }, {
    "text": "_. Betcha can't have just one!",
    "pick": 1
  }, {
    "text": "War!<br><br>What is it good for?",
    "pick": 1
  }, {
    "text": "BILLY MAYS HERE FOR _.",
    "pick": 1
  }, {
    "text": "_. High five, bro.",
    "pick": 1
  }, {
    "text": "During sex, I like to think about _.",
    "pick": 1
  }, {
    "text": "What did I bring back from Mexico?",
    "pick": 1
  }, {
    "text": "What are my parents hiding from me?",
    "pick": 1
  }, {
    "text": "What will always get you laid?",
    "pick": 1
  }, {
    "text": "What would grandma find disturbing, yet oddly charming?",
    "pick": 1
  }, {
    "text": "What did the U.S. airdrop to the children of Afghanistan?",
    "pick": 1
  }, {
    "text": "What helps Obama unwind?",
    "pick": 1
  }, {
    "text": "What's there a ton of in heaven?",
    "pick": 1
  }, {
    "text": "Major League Baseball has banned _ for giving players an unfair advantage.",
    "pick": 1
  }, {
    "text": "When I am a billionaire, I shall erect a 50-foot statue to commemorate _.",
    "pick": 1
  }, {
    "text": "What's the new fad diet?",
    "pick": 1
  }, {
    "text": "When I am the President of the United States, I will create the Department of _.",
    "pick": 1
  }, {
    "text": "_. It's a trap!",
    "pick": 1
  }, {
    "text": "How am I maintaining my relationship status?",
    "pick": 1
  }, {
    "text": "What will I bring back in time to convince people that I am a powerful wizard?",
    "pick": 1
  }, {
    "text": "While the United States raced the Soviet Union to the moon, the Mexican government funneled millions of pesos into research on _.",
    "pick": 1
  }, {
    "text": "Coming to Broadway this season, _: The Musical.",
    "pick": 1
  }, {
    "text": "What's my secret power?",
    "pick": 1
  }, {
    "text": "What gives me uncontrollable gas?",
    "pick": 1
  }, {
    "text": "But before I kill you, Mr. Bond, I must show you _.",
    "pick": 1
  }, {
    "text": "What never fails to liven up the party?",
    "pick": 1
  }, {
    "text": "What am I giving up for Lent?",
    "pick": 1
  }, {
    "text": "What do old people smell like? ",
    "pick": 1
  }, {
    "text": "The class field trip was completely ruined by _.",
    "pick": 1
  }, {
    "text": "When Pharaoh remained unmoved, Moses called down a plague of _.",
    "pick": 1
  }, {
    "text": "I do not know with which weapons World War III will be fought, but World War IV will be fought with _.",
    "pick": 1
  }, {
    "text": "What's Teach for America using to inspire inner city students to succeed?",
    "pick": 1
  }, {
    "text": "In Michael Jackson's final moments, he thought about _.",
    "pick": 1
  }, {
    "text": "Why do I hurt all over?",
    "pick": 1
  }, {
    "text": "Studies show that lab rats navigate mazes 50% faster after being exposed to _.",
    "pick": 1
  }, {
    "text": "Why am I sticky?",
    "pick": 1
  }, {
    "text": "What's my anti-drug?",
    "pick": 1
  }, {
    "text": "And the Academy Award for _ goes to _.",
    "pick": 2
  }, {
    "text": "For my next trick, I will pull _ out of _.",
    "pick": 2
  }, {
    "text": "_: Good to the last drop.",
    "pick": 1
  }, {
    "text": "What did Vin Diesel eat for dinner?",
    "pick": 1
  }, {
    "text": "_: kid-tested, mother-approved.",
    "pick": 1
  }, {
    "text": "What gets better with age?",
    "pick": 1
  }, {
    "text": "I never truly understood _ until I encountered _.",
    "pick": 2
  }, {
    "text": "Rumor has it that Vladimir Putin's favorite delicacy is _ stuffed with _.",
    "pick": 2
  }, {
    "text": "Lifetime&reg; presents _, the story of _.",
    "pick": 2
  }, {
    "text": "Make a haiku.",
    "pick": 3
  }, {
    "text": "In M. Night Shyamalan's new movie, Bruce Willis discovers that _ had really been _ all along.",
    "pick": 2
  }, {
    "text": "_ is a slippery slope that leads to _.",
    "pick": 2
  }, {
    "text": "In a world ravaged by _, our only solace is _.",
    "pick": 2
  }, {
    "text": "That's right, I killed _. How, you ask? _.",
    "pick": 2
  }, {
    "text": "When I was tripping on acid, _ turned into _.",
    "pick": 2
  }, {
    "text": "_ + _ = _.",
    "pick": 3
  }, {
    "text": "What's the next superhero\/sidekick duo?",
    "pick": 2
  }, {
    "text": "Dear Abby,<br><br>I'm having some trouble with _ and would like your advice.",
    "pick": 1
  }, {
    "text": "After the earthquake, Sean Penn brought _ to the people of Haiti.",
    "pick": 1
  }, {
    "text": "In L.A. County Jail, word is you can trade 200 cigarettes for _.",
    "pick": 1
  }, {
    "text": "Maybe she's born with it. Maybe it's _.",
    "pick": 1
  }, {
    "text": "Life for American Indians was forever changed when the White Man introduced them to _.",
    "pick": 1
  }, {
    "text": "Next on ESPN2, the World Series of _.",
    "pick": 1
  }, {
    "text": "Step 1: _. Step 2: _. Step 3: Profit.",
    "pick": 2
  }, {
    "text": "Here is the church<br>Here is the steeple<br>Open the doors<br>And there is _.",
    "pick": 1
  }, {
    "text": "How did I lose my virginity?",
    "pick": 1
  }, {
    "text": "During his childhood, Salvador Dal&iacute; produced hundreds of paintings of _.",
    "pick": 1
  }, {
    "text": "In 1,000 years, when paper money is a distant memory, how will we pay for goods and services?",
    "pick": 1
  }, {
    "text": "What don't you want to find in your Kung Pao chicken?",
    "pick": 1
  }, {
    "text": "The Smithsonian Museum of Natural History has just opened an exhibit on _.",
    "pick": 1
  }, {
    "text": "Daddy, why is Mommy crying?",
    "pick": 1
  }, {
    "text": "What brought the orgy to a grinding halt?",
    "pick": 1
  }, {
    "text": "When I pooped, what came out of my butt?",
    "pick": 1
  }, {
    "text": "In the distant future, historians will agree that _ marked the beginning of America's decline.",
    "pick": 1
  }, {
    "text": "What's the gift that keeps on giving?",
    "pick": 1
  }, {
    "text": "This season on Man vs. Wild, Bear Grylls must survive in the depths of the Amazon with only _ and his wits.",
    "pick": 1
  }, {
    "text": "Michael Bay's new three-hour action epic pits _ against _.",
    "pick": 2
  }, {
    "text": "And I would have gotten away with it, too, if it hadn't been for _!",
    "pick": 1
  }, {
    "text": "In a pinch, _ can be a suitable substitute for _.",
    "pick": 2
  }, {
    "text": "What has been making life difficult at the nudist colony?",
    "pick": 1
  }, {
    "text": "Science will never explain the origin of _.",
    "pick": 1
  }, {
    "text": "In Rome, there are whisperings that the Vatican has a secret room devoted to _.",
    "pick": 1
  }, {
    "text": "I learned the hard way that you can't cheer up a grieving friend with _.",
    "pick": 1
  }, {
    "text": "When all else fails, I can always masturbate to _.",
    "pick": 1
  }, {
    "text": "An international tribunal has found _ guilty of _.",
    "pick": 2
  }, {
    "text": "In its new tourism campaign, Detroit proudly proclaims that it has finally eliminated _.",
    "pick": 1
  }, {
    "text": "In his new self-produced album, Kanye West raps over the sounds of _.",
    "pick": 1
  }, {
    "text": "The socialist governments of Scandinavia have declared that access to _ is a basic human right.",
    "pick": 1
  }, {
    "text": "He who controls _ controls the world.",
    "pick": 1
  }, {
    "text": "Dear Sir or Madam, We regret to inform you that the Office of _ has denied your request for _.",
    "pick": 2
  }, {
    "text": "The CIA now interrogates enemy agents by repeatedly subjecting them to _.",
    "pick": 1
  }, {
    "text": "_ would be woefully incomplete without _.",
    "pick": 2
  }, {
    "text": "During his midlife crisis, my dad got really into _.",
    "pick": 1
  }, {
    "text": "Before I run for president, I must destroy all evidence of my involvement with _.",
    "pick": 1
  }, {
    "text": "My new favorite porn star is Joey \"_\" McGee.",
    "pick": 1
  }, {
    "text": "In his newest and most difficult stunt, David Blaine must escape from _.",
    "pick": 1
  }, {
    "text": "This is your captain speaking. Fasten your seatbelts and prepare for _.",
    "pick": 1
  }, {
    "text": "My mom freaked out when she looked at my browser history and found _.com\/_.",
    "pick": 2
  }, {
    "text": "The Five Stages of Grief: denial, anger, bargaining, _, acceptance.",
    "pick": 1
  }, {
    "text": "Members of New York's social elite are paying thousands of dollars just to experience _.",
    "pick": 1
  }, {
    "text": "I went from _ to _, all thanks to _.",
    "pick": 3
  }, {
    "text": "Little Miss Muffet Sat on a tuffet, Eating her curds and _.",
    "pick": 1
  }, {
    "text": "This month's Cosmo: \"Spice up your sex life by bringing _ into the bedroom.\"",
    "pick": 1
  }, {
    "text": "If God didn't want us to enjoy _, he wouldn't have given us _.",
    "pick": 2
  }, {
    "text": "My country, 'tis of thee, sweet land of _.",
    "pick": 1
  }, {
    "text": "After months of debate, the Occupy Wall Street General Assembly could only agree on \"More _!\"",
    "pick": 1
  }, {
    "text": "I spent my whole life working toward _, only to have it ruined by _.",
    "pick": 2
  }, {
    "text": "Next time on Dr. Phil: How to talk to your child about _.",
    "pick": 1
  }, {
    "text": "Only two things in life are certain: death and _.",
    "pick": 1
  }, {
    "text": "Everyone down on the ground! We don't want to hurt anyone. We're just here for _.",
    "pick": 1
  }, {
    "text": "The healing process began when I joined a support group for victims of _.",
    "pick": 1
  }, {
    "text": "The votes are in, and the new high school mascot is _.",
    "pick": 1
  }, {
    "text": "Charades was ruined for me forever when my mom had to act out _.",
    "pick": 1
  }, {
    "text": "Before _, all we had was _.",
    "pick": 2
  }, {
    "text": "Tonight on 20\/20: What you don't know about _ could kill you.",
    "pick": 1
  }, {
    "text": "You haven't truly lived until you've experienced _ and _ at the same time.",
    "pick": 2
  }, {
    "text": "Hey baby, come back to my place and I'll show you _.",
    "pick": 1
  }, {
    "text": "My gym teacher got fired for adding _ to the obstacle course.",
    "pick": 1
  }, {
    "text": "Finally! A service that delivers _ right to your door.",
    "pick": 1
  }, {
    "text": "To prepare for his upcoming role, Daniel Day-Lewis immersed himself in the world of _.",
    "pick": 1
  }, {
    "text": "My life is ruled by a vicious cycle of _ and _.",
    "pick": 2
  }, {
    "text": "During high school, I never really fit in until I found _ club.",
    "pick": 1
  }, {
    "text": "Money can't buy me love, but it can buy me _.",
    "pick": 1
  }, {
    "text": "Listen, son. If you want to get involved with _, I won't stop you. Just steer clear of _.",
    "pick": 2
  }, {
    "text": "A successful job interview begins with a firm handshake and ends with _.",
    "pick": 1
  }, {
    "text": "Call the law offices of Goldstein &amp; Goldstein, because no one should have to tolerate _ in the workplace.",
    "pick": 1
  }, {
    "text": "Lovin' you is easy 'cause you're _.",
    "pick": 1
  }, {
    "text": "The blind date was going horribly until we discovered our shared interest in _.",
    "pick": 1
  }, {
    "text": "What left this stain on my couch?",
    "pick": 1
  }, {
    "text": "Turns out that _-Man was neither the hero we needed nor wanted.",
    "pick": 1
  }, {
    "text": "After months of practice with _, I think I'm finally ready for _.",
    "pick": 2
  }, {
    "text": "In the seventh circle of Hell, sinners must endure _ for all eternity.",
    "pick": 1
  }, {
    "text": "As part of his daily regimen, Anderson Cooper sets aside 15 minutes for _.",
    "pick": 1
  }, {
    "text": "When you get right down to it, _ is just _.",
    "pick": 2
  }, {
    "text": "Having problems with _? Try _!",
    "pick": 2
  }, {
    "text": "And what did <i>you<\/i> bring for show and tell?",
    "pick": 1
  }, {
    "text": "I'm not like the rest of you. I'm too rich and busy for _.",
    "pick": 1
  }, {
    "text": "With enough time and pressure, _ will turn into _.",
    "pick": 2
  }, {
    "text": "_: Hours of fun. Easy to use. Perfect for _!",
    "pick": 2
  }, {
    "text": "_. Awesome in theory, kind of a mess in practice.",
    "pick": 1
  }, {
    "text": "As part of his contract, Prince won't perform without _ in his dressing room.",
    "pick": 1
  }, {
    "text": "Man, this is bullshit. Fuck _.",
    "pick": 1
  }, {
    "text": "Dear Leader Kim Jong-un,<br>our village praises your infinite wisdom with a humble offering of _.",
    "pick": 1
  }, {
    "text": "_ may pass, but _ will last forever.",
    "pick": 2
  }, {
    "text": "She's up all night for good fun.<br>I'm up all night for _.",
    "pick": 1
  }, {
    "text": "Alright, bros. Our frat house is condemned, and all the hot slampieces are over at Gamma Phi. The time has come to commence Operation _.",
    "pick": 1
  }, {
    "text": "The Japanese have developed a smaller, more efficient version of _.",
    "pick": 1
  }, {
    "text": "In return for my soul, the Devil promised me _, but all I got was _.",
    "pick": 2
  }, {
    "text": "You guys, I saw this crazy movie last night. It opens on _, and then there's some stuff about _, and then it ends with _.",
    "pick": 3
  }, {
    "text": "_ will never be the same after _.",
    "pick": 2
  }, {
    "text": "Wes Anderson's new film tells the story of a precocious child coming to terms with _.",
    "pick": 1
  }, {
    "text": "In the beginning, there was _.<br>And the Lord said, \"Let there be _.\"",
    "pick": 2
  }, {
    "text": "What's fun until it gets weird?",
    "pick": 1
  }, {
    "text": "We never did find _, but along the way we sure learned a lot about _.",
    "pick": 2
  }, {
    "text": "You've seen the bearded lady!<br>You've seen the ring of fire!<br>Now, ladies and gentlemen, feast your eyes upon _!",
    "pick": 1
  }, {
    "text": "How am I compensating for my tiny penis?",
    "pick": 1
  }, {
    "text": "I'm sorry, sir, but we don't allow _ at the country club.",
    "pick": 1
  }, {
    "text": "2 AM in the city that never sleeps. The door swings open and <i>she<\/i> walks in, legs up to here. Something in her eyes tells me she's looking for _.",
    "pick": 1
  }, {
    "text": "As king, how will I keep the peasants in line?",
    "pick": 1
  }, {
    "text": "Oprah's book of the month is \"_ For _: A Story of Hope.\"",
    "pick": 2
  }, {
    "text": "Do <i>not<\/i> fuck with me! I am literally _ right now.",
    "pick": 1
  }, {
    "text": "Adventure.<br>Romance.<br>_.<br><br>From Paramount Pictures, \"_.\"",
    "pick": 2
  }, {
    "text": "I am become _, destroyer of _!",
    "pick": 2
  }, {
    "text": "It lurks in the night. It hungers for flesh. This summer, no one is safe from _.",
    "pick": 1
  }, {
    "text": "If you can't handle _, you'd better stay away from _.",
    "pick": 2
  }, {
    "text": "This is the prime of my life. I'm young, hot, and full of _.",
    "pick": 1
  }, {
    "text": "I'm pretty sure I'm high right now, because I'm absolutely mesmerized by _.",
    "pick": 1
  }, {
    "text": "This year's hottest album is \"_\" by _.",
    "pick": 2
  }, {
    "text": "Every step towards _ gets me a little closer to _.",
    "pick": 2
  }, {
    "text": "Forget everything you know about _, because now we've supercharged it with _!",
    "pick": 2
  }, {
    "text": "Honey, I have a new role-play I want to try tonight! You can be _, and I'll be _.",
    "pick": 2
  }, {
    "text": "Do the Dew &reg; with our most extreme flavor yet! Get ready for Mountain Dew _!",
    "pick": 1
  }, {
    "text": "Armani suit: $1,000. Dinner for two at that swanky restaurant: $300. The look on her face when you surprise her with _: priceless.",
    "pick": 1
  }, {
    "text": "In his new action comedy, Jackie Chan must fend off ninjas while also dealing with _.",
    "pick": 1
  }, {
    "text": "Well what do you have to say for yourself, Casey? This is the third time you've been sent to the principal's office for _.",
    "pick": 1
  }, {
    "text": "Here at the Academy for Gifted Children, we allow students to explore _ at their own pace.",
    "pick": 1
  }, {
    "text": "Heed my voice, mortals! I am the god of _, and I will not tolerate _!",
    "pick": 2
  }, {
    "text": "I don't mean to brag, but they call me the Michael Jordan of _.",
    "pick": 1
  }, {
    "text": "Why am I broke?",
    "pick": 1
  }, {
    "text": "Help me doctor, I've got _ in my butt!",
    "pick": 1
  }, {
    "text": "Hi MTV! My name is Kendra, I live in Malibu, I'm into _, and I love to have a good time.",
    "pick": 1
  }, {
    "text": "Patient presents with _. Likely a result of _.",
    "pick": 2
  }, {
    "text": "Life's pretty tough in the fast lane. That's why I never leave the house without _.",
    "pick": 1
  }, {
    "text": "What's making things awkward in the sauna?",
    "pick": 1
  }, {
    "text": "Get ready for the movie of the summer! One cop plays by the book. The other's only interested in one thing: _.",
    "pick": 1
  }, {
    "text": "Having the worst day EVER. #_",
    "pick": 1
  }, {
    "text": "In his farewell address, George Washington famously warned Americans about the dangers of _.",
    "pick": 1
  }, {
    "text": "Don't forget! Beginning this week, Casual Friday will officially become \"_ Friday.\"",
    "pick": 1
  }, {
    "text": "What killed my boner?",
    "pick": 1
  }, {
    "text": "Yo' mama so fat she _!",
    "pick": 1
  }, {
    "text": "Well if _ is good enough for _, it's good enough for me.",
    "pick": 2
  }, {
    "text": "Hi, this is Jim from accounting. We noticed a $1,200 charge labeled \"_\". Can you explain?",
    "pick": 1
  }, {
    "text": "Do you lack energy? Does it sometimes feel like the whole world is _? Zoloft.&reg;",
    "pick": 1
  }, {
    "text": "WHOOO! God damn I love _!",
    "pick": 1
  }, {
    "text": "Now in bookstores: \"The Audacity of _\", by Barack Obama.",
    "pick": 1
  }, {
    "text": "And today's soup is Cream of _.",
    "pick": 1
  }, {
    "pick": 1,
    "text": "I work my ass off all day for this family, and this is what I come home to? _!?"
  }, {
    "pick": 1,
    "text": "I have a strict policy. First date, dinner. Second date, kiss. Third date, _."
  }, {
    "pick": 1,
    "text": "When I was a kid, we used to play Cowboys and _."
  }, {
    "pick": 1,
    "text": "This is America. If you don't work hard, you don't succeed. I don't care if you're black, white, purple, or _."
  }, {
    "pick": 1,
    "text": "You Won't Believe These 15 Hilarious _ Bloopers!"
  }, {
    "pick": 1,
    "text": "James is a lonely boy. But when he discovers a secret door in his attic, he meets a magical new friend: _."
  }, {
    "pick": 1,
    "text": "Don't worry kid. It gets better. I've been living with _ for 20 years."
  }, {
    "pick": 1,
    "text": "My grandfather worked his way up from nothing. When he came to this country, all he had was the shoes on his feet and _."
  }, {
    "pick": 1,
    "text": "Behind every powerful man is _."
  }, {
    "pick": 1,
    "text": "You are not alone. Millions of Americans struggle with _ every day."
  }, {
    "pick": 1,
    "text": "Come to Dubai, where you can relax in our world famous spas, experience the nightlife, or simply enjoy _ by the poolside."
  }, {
    "pick": 1,
    "text": "\"This is madness.\" \"No, THIS IS _!\""
  }, {
    "pick": 1,
    "text": "Listen Gary, I like you. But if you want that corner office, you're going to have to show me _."
  }, {
    "pick": 1,
    "text": "I went to the desert and ate of the peyote cactus. Turns out my spirit animal is _."
  }, {
    "pick": 1,
    "text": "And would you like those buffalo wings mild, hot, or _?"
  }, {
    "pick": 1,
    "text": "The six things I could never do without: oxygen, Facebook, chocolate, Netflix, friends, and _ LOL!"
  }, {
    "pick": 1,
    "text": "Why won't you make love to me anymore? Is it _?"
  }, {
    "pick": 1,
    "text": "Puberty is a time of change. You might notice hair growing in new places. You might develop an interest in _. This is normal."
  }, {
    "pick": 1,
    "text": "I'm sorry, Mrs. Chen, but there was nothing we could do. At 4:15 this morning, your son succumbed to _."
  }, {
    "pick": 1,
    "text": "I'm Miss Tennessee, and if I could make the world better by changing one thing, I would get rid of _."
  }, {
    "pick": 1,
    "text": "Tonight we will have sex. And afterwards, If you'd like, a little bit of _."
  }, {
    "pick": 1,
    "text": "Everybody join hands and close your eyes. Do you sense that? That's the presence of _ in this room."
  }, {
    "pick": 1,
    "text": "To become a true Yanomamo warrior, you must prove that you can withstand _ without crying out."
  }, {
    "pick": 1,
    "text": "Y'all ready to get this thing started? I'm Nick Cannon, and this is America's Got _."
  }, {
    "pick": 1,
    "text": "If you had to describe the Card Czar, using only one of the cards in your hand, which one would it be?"
  }, {
    "pick": 2,
    "text": "In line with our predictions, we find a robust correlation between _ and _ (p&gt;.05)."
  }, {
    "pick": 1,
    "text": "In what's being hailed as a major breakthrough, scientists have synthesized _ in the lab."
  }, {
    "pick": 1,
    "text": "A study published in Nature this week found that _ is good for you in small doses."
  }, {
    "pick": 2,
    "text": "In an attempt to recreate conditions just after the Big Bang, physicists at the LHC are observing collisions between _ and _."
  }, {
    "pick": 1,
    "text": "What really killed the dinosaurs?"
  }, {
    "pick": 1,
    "text": "Hey there, Young Scientists! Put on your labcoats and strap on your safety goggles, because today we're learning about _!"
  }, {
    "pick": 2,
    "text": "Today on MythBusters, we found out how long _ can withstand _."
  }, {
    "pick": 1,
    "text": "You can't wait forever. It's time to talk to your doctor about _."
  }, {
    "pick": 1,
    "text": "The Westboro Baptist Church is now picketing soldiers' funerals with signs that read 'GOD HATES _.'"
  }, {
    "pick": 1,
    "text": "What are two cards in your hand that you want to get rid of?"
  }, {
    "pick": 1,
    "text": "The elders of the Ibo tribe of Nigeria recommend _ as a cure for impotence."
  }, {
    "pick": 1,
    "text": "My name is Inigo Montoya. You killed my father. Prepare for _."
  }, {
    "pick": 1,
    "text": "From WBEZ Chicago, it's This American Life. Today on our program, _. Stay with us."
  }, {
    "pick": 1,
    "text": "What do you see?"
  }, {
    "pick": 1,
    "text": "Sir, we found you passed out naked on the side of the road. What's the last thing you remember?"
  }, {
    "text": "Air Canada guidelines now prohibit _ on airplanes.",
    "pick": 1
  }, {
    "text": "O Canada, we stand on guard for _.",
    "pick": 1
  }, {
    "text": "CTV presents _, the story of _.",
    "pick": 2
  }, {
    "text": "In an attempt to reach a wider audience, the Royal Ontario Museum has opened an interactive exhibit on _.",
    "pick": 1
  }, {
    "text": "What's the Canadian government using to inspire rural students to succeed?",
    "pick": 1
  }, {
    "text": "Hey, Susie. I know your job is _ but can you just grab me _? Thanks.",
    "pick": 2
  }, {
    "text": "This month in Cosmo: how to give your man _ at the expense of _.",
    "pick": 2
  }, {
    "text": "Are you there, God? It's me, _",
    "pick": 1
  }, {
    "text": "50 Shades of _.",
    "pick": 1
  }, {
    "text": "It's not length, it's _.",
    "pick": 1
  }, {
    "text": "Whatever, Peeta. You'll never understand my struggle with _.",
    "pick": 1
  }, {
    "text": "Men are from _, women are from _.",
    "pick": 2
  }, {
    "text": "Why does the Komen Foundation hate Planned Parenthood?",
    "pick": 1
  }, {
    "text": "Math is hard. Let's go _!",
    "pick": 1
  }, {
    "text": "The latest proposal in the Texas legislature is to take away _ from women.",
    "pick": 1
  }, {
    "text": "If you don't mind my asking, how *do* lesbians have sex?",
    "pick": 1
  }, {
    "text": "In her next romcom, Katherine Heigl plays a woman who falls in love with her boss's _.",
    "pick": 1
  }, {
    "text": "The Pantone color of the year is inspired by _.",
    "pick": 1
  }, {
    "text": "What is Olivia Pope's secret to removing red wine stains from white clothes?",
    "pick": 1
  }, {
    "text": "Why exactly was Alanis so mad at Uncle Joey?",
    "pick": 1
  }, {
    "text": "Why do men on the Internet send me pictures of _?",
    "pick": 1
  }, {
    "text": "What's my weapon of choice in the \"War on Women\"?",
    "pick": 1
  }, {
    "text": "What's Seth MacFarlane's problem?",
    "pick": 1
  }, {
    "text": "I couldn't help but wonder: was it Mr. Big, or was it _?",
    "pick": 1
  }, {
    "text": "What fell into my bra?",
    "pick": 1
  }, {
    "text": "What's my preferred method of contraception?",
    "pick": 1
  }, {
    "text": "Sofia Coppola's new film focuses on a wealthy young white woman feeling alienated by _.",
    "pick": 1
  }, {
    "text": "_: the Tori Amos song that changed my life",
    "pick": 1
  }, {
    "text": "Something old, something new, something borrowed, and _.",
    "pick": 1
  }, {
    "text": "Why can't we have nice things?",
    "pick": 1
  }, {
    "text": "Between love and madness lies _.",
    "pick": 1
  }, {
    "text": "Instead of chess, the Grim Reaper now gambles for your soul with a game of _.",
    "pick": 1
  }, {
    "text": "My father gave his life fighting to protect _ from _.",
    "pick": 2
  }, {
    "text": "Why is my throat sore?",
    "pick": 1
  }, {
    "text": "_ sparked a city-wide riot that only ended with _.",
    "pick": 2
  }, {
    "text": "I'm very sorry Mrs. Smith, but Little Billy has tested positive for _.",
    "pick": 1
  }, {
    "text": "Instead of beating them, Chris Brown now does _ to women.",
    "pick": 1
  }, {
    "text": "Instead of cutting, trendy young emo girls now engage in _.",
    "pick": 1
  }, {
    "text": "The definition of rock bottom is gambling away _.",
    "pick": 1
  }, {
    "text": "The Mayan prophecies really heralded the coming of _ in 2012.",
    "pick": 1
  }, {
    "text": "The next US election will be fought on the key issues of _ against _.",
    "pick": 2
  }, {
    "text": "When I was 10 I wrote to Santa wishing for _.",
    "pick": 1
  }, {
    "text": "Where or How I met my last signifigant other: _.",
    "pick": 1
  }, {
    "text": "_, Never leave home without it.",
    "pick": 1
  }, {
    "text": "_. This is my fetish.",
    "pick": 1
  }, {
    "text": "David Icke's newest conspiracy theory states that _ caused _.",
    "pick": 2
  }, {
    "text": "I did _ so you don't have to!",
    "pick": 1
  }, {
    "text": "I need your clothes, your bike, and _.",
    "pick": 1
  }, {
    "text": "In a new Cold War retro movie, the red menace tries to conquer the world through the cunning use of _.",
    "pick": 1
  }, {
    "text": "In college, our lecturer made us write a report comparing _ to _.",
    "pick": 2
  }, {
    "text": "In The Hangover part 3, those four guys have to deal with _, _, and _.",
    "pick": 3
  }, {
    "text": "My zombie survival kit includes food, water, and _.",
    "pick": 1
  }, {
    "text": "The way to a man's heart is through _.",
    "pick": 1
  }, {
    "text": "What was the theme of my second wedding?",
    "pick": 1
  }, {
    "text": "What's the newest Japanese craze to head West?",
    "pick": 1
  }, {
    "text": "Everybody loves _.",
    "pick": 1
  }, {
    "text": "I can only express myself through _.",
    "pick": 1
  }, {
    "text": "My new porn DVD was completely ruined by the inclusion of _",
    "pick": 1
  }, {
    "text": "My three wishes will be for _, _, and _.",
    "pick": 3
  }, {
    "text": "The latest horrifying school shooting was inspired by _.",
    "pick": 1
  }, {
    "text": "I got fired because of my not-so-secret obsession over _.",
    "pick": 1
  }, {
    "text": "My new favourite sexual position is _",
    "pick": 1
  }, {
    "text": "Who let the dogs out?",
    "pick": 1
  }, {
    "text": "In his next movie, Will Smith saves the world from _.",
    "pick": 1
  }, {
    "text": "_ and _ are the new hot couple.",
    "pick": 2
  }, {
    "text": "When North Korea gets _, it will be the end of the world.",
    "pick": 1
  }, {
    "text": "Plan a three course meal.",
    "pick": 3
  }, {
    "text": "Tastes like _.",
    "pick": 1
  }, {
    "text": "What will Xyzzy take over the world with?",
    "pick": 1
  }, {
    "text": "Dustin Browder demands more _ in StarCraft&reg;.",
    "pick": 1
  }, {
    "text": "Welcome to my secret lair on _.",
    "pick": 1
  }, {
    "text": "Praise _!",
    "pick": 1
  }, {
    "text": "What can you always find in between the couch cushions?",
    "pick": 1
  }, {
    "text": "What is the next great Kickstarter project?",
    "pick": 1
  }, {
    "text": "My life for _!",
    "pick": 1
  }, {
    "text": "In a news conference, Obama pulled out _, to everyone's surprise.",
    "pick": 1
  }, {
    "text": "Nights filled with _.",
    "pick": 1
  }, {
    "text": "_ Game of the Year Edition.",
    "pick": 1
  }, {
    "text": "What was going through Osama Bin Laden's head before he died?",
    "pick": 1
  }, {
    "text": "The lion, the witch, and _.",
    "pick": 1
  }, {
    "text": "In the next episode, SpongeBob gets introduced to _. ",
    "pick": 1
  }, {
    "text": "Justin Beiber's new song is all about _.",
    "pick": 1
  }, {
    "text": "Keith Richards enjoys _ on his food.",
    "pick": 1
  }, {
    "text": "The new fad diet is all about making people do _ and eat _.",
    "pick": 2
  }, {
    "text": "Grand Theft Auto&trade;: _.",
    "pick": 1
  }, {
    "text": "The victim was found with _.",
    "pick": 1
  }, {
    "text": "I whip my _ back and forth.",
    "pick": 1
  }, {
    "text": "What is love without _?",
    "pick": 1
  }, {
    "text": "_ 2012.",
    "pick": 1
  }, {
    "text": "What is Curious George so curious about?",
    "pick": 1
  }, {
    "text": "What is the next big sideshow attraction?",
    "pick": 1
  }, {
    "text": "Lady Gaga has revealed her new dress will be made of _.",
    "pick": 1
  }, {
    "text": "_ ruined many people's childhood.",
    "pick": 1
  }, {
    "text": "Who needs college when you have _.",
    "pick": 1
  }, {
    "text": "When short on money, you can always _.",
    "pick": 1
  }, {
    "text": "What is literally worse than Hitler?",
    "pick": 1
  }, {
    "text": "I wouldn't fuck _ with _'s dick.",
    "pick": 2
  }, {
    "text": "The next Assassin's Creed game will take place in _.",
    "pick": 1
  }, {
    "text": "In the next Punch Out!!, _ will be the secret final boss.",
    "pick": 1
  }, {
    "text": "One does not simply walk into _.",
    "pick": 1
  }, {
    "text": "The next pokemon will combine _ and _.",
    "pick": 2
  }, {
    "text": "Instead of playing Cards Against Humanity, you could be _.",
    "pick": 1
  }, {
    "text": "Who is GLaDOS's next test subject?",
    "pick": 1
  }, {
    "text": "In his second term, Obama will rid America of _.",
    "pick": 1
  }, {
    "text": "I never thought _ would be so enjoyable.",
    "pick": 1
  }, {
    "text": "In the future, _ will fuel our cars.",
    "pick": 1
  }, {
    "text": "What is Japan's national pastime?",
    "pick": 1
  }, {
    "text": "I've got the whole world in my _.",
    "pick": 1
  }, {
    "text": "What is the answer to life's question?",
    "pick": 1
  }, {
    "text": "It's difficult to explain to friends and family why I know so much about _.",
    "pick": 1
  }, {
    "text": "Who knew I'd be able to make a living off of _?",
    "pick": 1
  }, {
    "text": "Long story short, I ended up with _ in my ass.",
    "pick": 1
  }, {
    "text": "At first I couldn't understand _, but now it's my biggest kink.",
    "pick": 1
  }, {
    "text": "I can't believe I spent most of my paycheck on _.",
    "pick": 1
  }, {
    "text": "_, by Bad Dragon&reg;.",
    "pick": 1
  }, {
    "text": "_ are so goddamn cool.",
    "pick": 1
  }, {
    "text": "Once I started roleplaying _, it was all downhill from there.",
    "pick": 1
  }, {
    "text": "Don't knock _ until you've tried it.",
    "pick": 1
  }, {
    "text": "_ looks pretty in all the art, but have you seen one in real life?",
    "pick": 1
  }, {
    "text": "My landlord had a lot of uncomfortable questions for me when when he found _ in my bedroom while I was at work.",
    "pick": 1
  }, {
    "text": "Everyone on this site has such strong opinions about _.",
    "pick": 1
  }, {
    "text": "I realized they were a furry when they mentioned _.",
    "pick": 1
  }, {
    "text": "Whoa, I might fantasize about _, but I'd never actually go that far in real life.",
    "pick": 1
  }, {
    "text": "Realizing, too late, the implications of your interest in _ as a child.",
    "pick": 1
  }, {
    "text": "I've been into _ since before I hit puberty, I just didn't know what it meant.",
    "pick": 1
  }, {
    "text": "I'm no longer allowed near _ after the incident with _.",
    "pick": 2
  }, {
    "text": "It all started with _.",
    "pick": 1
  }, {
    "text": "I'll roleplay _, you can be _.",
    "pick": 2
  }, {
    "text": "Only my internet friends know that I fantasize about _.",
    "pick": 1
  }, {
    "text": "Everyone really just goes to the cons for _.",
    "pick": 1
  }, {
    "text": "My Original Character's name is _.",
    "pick": 1
  }, {
    "text": "My secret tumblr account where I post nothing but _.",
    "pick": 1
  }, {
    "text": "The panel I'm looking forward to most at AC this year is...",
    "pick": 1
  }, {
    "text": "If my parents ever found _, I'd probably be disowned.",
    "pick": 1
  }, {
    "text": "It's really hard not to laugh at _.",
    "pick": 1
  }, {
    "text": "The most recent item in my search history.",
    "pick": 1
  }, {
    "text": "_ ruined the fandom.",
    "pick": 1
  }, {
    "text": "I didn't believe the rumors about _, until I saw the videos.",
    "pick": 1
  }, {
    "text": "_. Yeah, that's a pretty interesting way to die.",
    "pick": 1
  }, {
    "text": "After being a furry for so long, I can never see _ without getting a little aroused.",
    "pick": 1
  }, {
    "text": "I knew I needed to leave the fandom when I realized I was _.",
    "pick": 1
  }, {
    "text": "Yeah, I know I have a lot of _ in my favorites, but I'm just here for the art.",
    "pick": 1
  }, {
    "text": "I never felt more accomplished than when I realized I could fit _ into my ass.",
    "pick": 1
  }, {
    "text": "Okay, _? Pretty much the cutest thing ever.",
    "pick": 1
  }, {
    "text": "I'm not a \"furry,\" I prefer to be called _.",
    "pick": 1
  }, {
    "text": "_ is my spirit animal.",
    "pick": 1
  }, {
    "text": "In my past life, I was _.",
    "pick": 1
  }, {
    "text": "I'm not even sad that I devote at least six hours of each day to _.",
    "pick": 1
  }, {
    "text": "_. This is what my life has come to.",
    "pick": 1
  }, {
    "text": "Fuck _, get _.",
    "pick": 2
  }, {
    "text": "_? Oh murr.",
    "pick": 1
  }, {
    "text": "I would bend over for _.",
    "pick": 1
  }, {
    "text": "I think having horns would make _ complicated.",
    "pick": 1
  }, {
    "text": "_? Oh, yeah, I could get my mouth around that.",
    "pick": 1
  }, {
    "text": "What wouldn't I fuck?",
    "pick": 1
  }, {
    "text": "When I thought I couldn't go any lower, I realized I would probably fuck _.",
    "pick": 1
  }, {
    "text": "I knew my boyfriend was a keeper when he said he'd try _, just for me.",
    "pick": 1
  }, {
    "text": "I've been waiting all year for _.",
    "pick": 1
  }, {
    "text": "I can't wait to meet up with my internet friends for _.",
    "pick": 1
  }, {
    "text": "Did you hear about the guy that smuggled _ into the hotel?",
    "pick": 1
  }, {
    "text": "I'm not even aroused by normal porn anymore, I can only get off to _ or _.",
    "pick": 2
  }, {
    "text": "No, look, you don't understand. I REALLY like _.",
    "pick": 1
  }, {
    "text": "I don't think my parents will ever accept that the real me is _.",
    "pick": 1
  }, {
    "text": "You can try to justify _ all you want, but you don't have to be _ to realize it's just plain wrong.",
    "pick": 2
  }, {
    "text": "I remember when _ was just getting started.",
    "pick": 1
  }, {
    "text": "_ are definitely the new huskies.",
    "pick": 1
  }, {
    "text": "The real reason I got into the fandom? _.",
    "pick": 1
  }, {
    "text": "_ is no substitute for social skills, but it's a start.",
    "pick": 1
  }, {
    "text": "_ is a shining example of what those with autism can really do.",
    "pick": 1
  }, {
    "text": "I don't know how we got on the subject of dragon cocks, but it probably started with _.",
    "pick": 1
  }, {
    "text": "Actually coming inside _.",
    "pick": 1
  }, {
    "text": "When no one else is around, sometimes I consider doing things with _.",
    "pick": 1
  }, {
    "text": "_: Horrible tragedy, or sexual opportunity?",
    "pick": 1
  }, {
    "text": "I'm about 50% _.",
    "pick": 1
  }, {
    "text": "I knew I had a problem when I had to sell _ to pay for _.",
    "pick": 2
  }, {
    "text": "The most pleasant surprise I've had this year.",
    "pick": 1
  }, {
    "text": "It's just that much creepier when 40-year-old men are into _.",
    "pick": 1
  }, {
    "text": "Jizzing all over _.",
    "pick": 1
  }, {
    "text": "Hey, you guys wanna come back to my place? I've got _ and _.",
    "pick": 2
  }, {
    "text": "It's a little worrying that I have to compare the size of _ to beverage containers.",
    "pick": 1
  }, {
    "text": "It's not bestiality, it's _.",
    "pick": 1
  }, {
    "text": "Everyone thinks that because I'm a furry, I'm into _. Unfortunately, they're right.",
    "pick": 1
  }, {
    "text": "While everyone else seems to have a deep, instinctual fear of _, it just turns me on.",
    "pick": 1
  }, {
    "text": "Lying about having _ to get donations, which you spend on _.",
    "pick": 2
  }, {
    "text": "If you like it, then you should put _ on it.",
    "pick": 1
  }, {
    "text": "My girlfriend won't let me do _.",
    "pick": 1
  }, {
    "text": "I'm only gay for _.",
    "pick": 1
  }, {
    "text": "Excuse you, I'm a were-_.",
    "pick": 1
  }, {
    "text": "My next fursuit will have _.",
    "pick": 1
  }, {
    "text": "I'm writing a porn comic about _ and _. ",
    "pick": 2
  }, {
    "text": "Is it weird that I want to rub my face on _?",
    "pick": 1
  }, {
    "text": "I never thought I'd be comfortable with _, but now it's pretty much the only thing I masturbate to.",
    "pick": 1
  }, {
    "text": "Everyone thinks they're so great, but the only thing they're good at drawing is _.",
    "pick": 1
  }, {
    "text": "They're just going to spend all that money on _.",
    "pick": 1
  }, {
    "text": "I tell everyone that I make my money off \"illustration,\" when really, I just draw _.",
    "pick": 1
  }, {
    "text": "Oh, you're an artist? Could you draw _ for me?",
    "pick": 1
  }, {
    "text": "I used to avoid talking about _, but now it's just a part of normal conversation with my friends.",
    "pick": 1
  }, {
    "text": "You sometimes wish you'd encounter _ while all alone, in the woods. With a bottle of lube.",
    "pick": 1
  }, {
    "text": "Most cats are _.",
    "pick": 1
  }, {
    "text": "Personals ad: Seeking a female who doesn't mind _, might also be willing to try a male if they're _.",
    "pick": 2
  }, {
    "text": "Taking pride in one's collection of _.",
    "pick": 1
  }, {
    "text": "I tell everyone I'm not a furry, but I've drawn a lot of _.",
    "pick": 1
  }, {
    "text": "My original species combines _ and _. It's called _.",
    "pick": 3
  }, {
    "text": "_. And now I'm bleeding.",
    "pick": 1
  }, {
    "text": "Suck my _.",
    "pick": 1
  }, {
    "text": "I also take _ as payment for commissions.",
    "pick": 1
  }, {
    "text": "It is my dream to be covered with _.",
    "pick": 1
  }, {
    "text": "_ fucking _. Now that's hot.",
    "pick": 2
  }, {
    "text": "Would you rather suck _, or get dicked by _?",
    "pick": 2
  }, {
    "text": "It never fails to liven up the workplace when you ask your coworkers if they'd rather have sex with _ or _.",
    "pick": 2
  }, {
    "text": "HELLO FURRIEND, HOWL ARE YOU DOING?",
    "pick": 1
  }, {
    "text": "What are the two worst cards in your hand right now?",
    "pick": 2
  }, {
    "text": "Nobody believes me when I tell that one story about walking in on _.",
    "pick": 1
  }, {
    "text": "You don't know who _ is? They're the one that draws _.",
    "pick": 2
  }],
  "whiteCards": ["Coat hanger abortions.", "Man meat.", "Autocannibalism.", "Vigorous jazz hands.", "Flightless birds.", "Pictures of boobs.", "Doing the right thing.", "The violation of our most basic human rights.", "Viagra&reg;.", "Self-loathing.", "Spectacular abs.", "A balanced breakfast.", "Roofies.", "Concealing a boner.", "Amputees.", "The Big Bang.", "Former President George W. Bush.", "The Rev. Dr. Martin Luther King, Jr.", "Smegma.", "Being marginalized.", "Cuddling.", "Laying an egg.", "The Pope.", "Aaron Burr.", "Genital piercings.", "Fingering.", "A bleached asshole.", "Horse meat.", "Fear itself.", "Science.", "Elderly Japanese men.", "Stranger danger.", "The terrorists.", "Praying the gay away.", "Same-sex ice dancing.", "Ethnic cleansing.", "Cheating in the Special Olympics.", "German dungeon porn.", "Bingeing and purging.", "Making a pouty face.", "William Shatner.", "Heteronormativity.", "Nickelback.", "Tom Cruise.", "The profoundly handicapped.", "The placenta.", "Chainsaws for hands.", "Arnold Schwarzenegger.", "An icepick lobotomy.", "Goblins.", "Object permanence.", "Dying.", "Foreskin.", "A falcon with a cap on its head.", "Hormone injections.", "Dying of dysentery.", "Sexy pillow fights.", "The invisible hand.", "A really cool hat.", "Sean Penn.", "Heartwarming orphans.", "The clitoris.", "The Three-Fifths compromise.", "A sad handjob.", "Men.", "Historically black colleges.", "A micropenis.", "Raptor attacks.", "Agriculture.", "Vikings.", "Pretending to care.", "The Underground Railroad.", "My humps.", "Being a dick to children.", "Geese.", "Bling.", "Sniffing glue.", "The South.", "An Oedipus complex.", "Eating all of the cookies before the AIDS bake-sale.", "Sexting.", "YOU MUST CONSTRUCT ADDITIONAL PYLONS.", "Mutually-assured destruction.", "Sunshine and rainbows.", "Count Chocula.", "Sharing needles.", "Being rich.", "Skeletor.", "A sausage festival.", "Michael Jackson.", "Emotions.", "Farting and walking away.", "The Chinese gymnastics team.", "Necrophilia.", "Spontaneous human combustion.", "Yeast.", "Leaving an awkward voicemail.", "Dick Cheney.", "White people.", "Penis envy.", "Teaching a robot to love.", "Sperm whales.", "Scrubbing under the folds.", "Panda sex.", "Whipping it out.", "Catapults.", "Masturbation.", "Natural selection.", "Opposable thumbs.", "A sassy black woman.", "AIDS.", "The KKK.", "Figgy pudding.", "Seppuku.", "Gandhi.", "Preteens.", "Toni Morrison's vagina.", "Five-Dollar Footlongs&trade;.", "Land mines.", "A sea of troubles.", "A zesty breakfast burrito.", "Christopher Walken.", "Friction.", "Balls.", "Dental dams.", "A can of whoop-ass.", "A tiny horse.", "Waiting 'til marriage.", "Authentic Mexican cuisine.", "Genghis Khan.", "Old-people smell.", "Feeding Rosie O'Donnell.", "Pixelated bukkake.", "Friends with benefits.", "The token minority.", "The Tempur-Pedic&reg; Swedish Sleep System&trade;.", "A thermonuclear detonation.", "Take-backsies.", "The Rapture.", "A cooler full of organs.", "Sweet, sweet vengeance.", "RoboCop.", "Keanu Reeves.", "Drinking alone.", "Giving 110%.", "Flesh-eating bacteria.", "The American Dream.", "Taking off your shirt.", "Me time.", "A murder most foul.", "The inevitable heat death of the universe.", "The folly of man.", "That thing that electrocutes your abs.", "Cards Against Humanity.", "Fiery poops.", "Poor people.", "Edible underpants.", "Britney Spears at 55.", "All-you-can-eat shrimp for $4.99.", "Pooping back and forth. Forever.", "Fancy Feast&reg;.", "Jewish fraternities.", "Being a motherfucking sorcerer.", "Pulling out.", "Picking up girls at the abortion clinic.", "The homosexual agenda.", "The Holy Bible.", "Passive-agression.", "Ronald Reagan.", "Vehicular manslaughter.", "Nipple blades.", "Assless chaps.", "Full frontal nudity.", "Hulk Hogan.", "Daddy issues.", "The hardworking Mexican.", "Natalie Portman.", "Waking up half-naked in a Denny's parking lot.", "God.", "Sean Connery.", "Saxophone solos.", "Gloryholes.", "The World of Warcraft.", "Homeless people.", "Scalping.", "Darth Vader.", "Eating the last known bison.", "Guys who don't call.", "Hot Pockets&reg;.", "A time travel paradox.", "The milk man.", "Testicular torsion.", "Dropping a chandelier on your enemies and riding the rope up.", "World peace.", "A salty surprise.", "Poorly-timed Holocaust jokes.", "Smallpox blankets.", "Licking things to claim them as your own.", "The heart of a child.", "Robert Downey, Jr.", "Lockjaw.", "Eugenics.", "A good sniff.", "Friendly fire.", "The taint; the grundle; the fleshy fun-bridge.", "Wearing underwear inside-out to avoid doing laundry.", "Hurricane Katrina.", "Free samples.", "Jerking off into a pool of children's tears.", "A foul mouth.", "The glass ceiling.", "Republicans.", "Explosions.", "Michelle Obama's arms.", "Getting really high.", "Attitude.", "Sarah Palin.", "The &Uuml;bermensch.", "Altar boys.", "My soul.", "My sex life.", "Pedophiles.", "72 virgins.", "Pabst Blue Ribbon.", "Domino's&trade; Oreo&trade; Dessert Pizza.", "A snapping turtle biting the tip of your penis.", "The Blood of Christ.", "Half-assed foreplay.", "My collection of high-tech sex toys.", "A middle-aged man on roller skates.", "Bitches.", "Bill Nye the Science Guy.", "Italians.", "A windmill full of corpses.", "Adderall&trade;.", "Crippling debt.", "A stray pube.", "Prancing.", "Passing a kidney stone.", "A brain tumor.", "Leprosy.", "Puppies!", "Bees?", "Frolicking.", "Repression.", "Road head.", "A bag of magic beans.", "An asymmetric boob job.", "Dead parents.", "Public ridicule.", "A mating display.", "A mime having a stroke.", "Stephen Hawking talking dirty.", "African children.", "Mouth herpes.", "Overcompensation.", "Riding off into the sunset.", "Being on fire.", "Tangled Slinkys.", "Civilian casualties.", "Auschwitz.", "My genitals.", "Not reciprocating oral sex.", "Lactation.", "Being fabulous.", "Shaquille O'Neal's acting career.", "My relationship status.", "Asians who aren't good at math.", "Alcoholism.", "Incest.", "Grave robbing.", "Hope.", "8 oz. of sweet Mexican black-tar heroin.", "Kids with ass cancer.", "Winking at old people.", "The Jews.", "Justin Bieber.", "Doin' it in the butt.", "A lifetime of sadness.", "The Hamburglar.", "Swooping.", "Classist undertones.", "New Age music.", "Not giving a shit about the Third World.", "The Kool-Aid Man.", "A hot mess.", "Tentacle porn.", "Lumberjack fantasies.", "The gays.", "Scientology.", "Estrogen.", "GoGurt&reg;.", "Judge Judy.", "Dick fingers.", "Racism.", "Surprise sex!", "Police brutality.", "Passable transvestites.", "The Virginia Tech Massacre.", "When you fart and a little bit comes out.", "Oompa-Loompas.", "A fetus.", "Obesity.", "Tasteful sideboob.", "Hot people.", "BATMAN!!!", "Black people.", "A gassy antelope.", "Sexual tension.", "Third base.", "Racially-biased SAT questions.", "Porn stars.", "A Super Soaker&trade; full of cat pee.", "Muhammed (Praise Be Unto Him).", "Puberty.", "A disappointing birthday party.", "An erection that lasts longer than four hours.", "White privilege.", "Getting so angry that you pop a boner.", "Wifely duties.", "Two midgets shitting into a bucket.", "Queefing.", "Wiping her butt.", "Golden showers.", "Barack Obama.", "Nazis.", "A robust mongoloid.", "An M. Night Shyamalan plot twist.", "Getting drunk on mouthwash.", "Lunchables&trade;.", "Women in yogurt commercials.", "John Wilkes Booth.", "Powerful thighs.", "Mr. Clean, right behind you.", "Multiple stab wounds.", "Cybernetic enhancements.", "Serfdom.", "Kanye West.", "Women's suffrage.", "Children on leashes.", "Harry Potter erotica.", "The Dance of the Sugar Plum Fairy.", "Lance Armstrong's missing testicle.", "Parting the Red Sea.", "The Amish.", "Dead babies.", "Child beauty pageants.", "AXE Body Spray.", "Centaurs.", "Copping a feel.", "Grandma.", "Famine.", "The Trail of Tears.", "The miracle of childbirth.", "Finger painting.", "A monkey smoking a cigar.", "The Make-A-Wish&reg; Foundation.", "Anal beads.", "The Force.", "Kamikaze pilots.", "Dry heaving.", "Active listening.", "Ghosts.", "The Hustle.", "Peeing a little bit.", "Another goddamn vampire movie.", "Shapeshifters.", "The Care Bear Stare.", "Hot cheese.", "A mopey zoo lion.", "A defective condom.", "Teenage pregnancy.", "A Bop It&trade;.", "Expecting a burp and vomiting on the floor.", "Horrifying laser hair removal accidents.", "Boogers.", "Unfathomable stupidity.", "Breaking out into song and dance.", "Soup that is too hot.", "Morgan Freeman's voice.", "Getting naked and watching Nickelodeon.", "MechaHitler.", "Flying sex snakes.", "The true meaning of Christmas.", "My inner demons.", "Pac-Man uncontrollably guzzling cum.", "My vagina.", "A homoerotic volleyball montage.", "Actually taking candy from a baby.", "Crystal meth.", "Exactly what you'd expect.", "Natural male enhancement.", "Passive-aggressive Post-it notes.", "Inappropriate yodeling.", "Lady Gaga.", "The Little Engine That Could.", "Vigilante justice.", "A death ray.", "Poor life choices.", "A gentle caress of the inner thigh.", "Embryonic stem cells.", "Nicolas Cage.", "Firing a rifle into the air while balls deep in a squealing hog.", "Switching to Geico&reg;.", "The chronic.", "Erectile dysfunction.", "Home video of Oprah sobbing into a Lean Cuisine&reg;.", "A bucket of fish heads.", "50,000 volts straight to the nipples.", "Being fat and stupid.", "Hospice care.", "A pyramid of severed heads.", "Getting married, having a few kids, buying some stuff, retiring to Florida, and dying.", "A subscription to Men's Fitness.", "Crucifixion.", "A micropig wearing a tiny raincoat and booties.", "Some god-damn peace and quiet.", "Used panties.", "A tribe of warrior women.", "The penny whistle solo from \"My Heart Will Go On.\"", "An oversized lollipop.", "Helplessly giggling at the mention of Hutus and Tutsis.", "Not wearing pants.", "Consensual sex.", "Her Majesty, Queen Elizabeth II.", "Funky fresh rhymes.", "The art of seduction.", "The Devil himself.", "Advice from a wise, old black man.", "Destroying the evidence.", "The light of a billion suns.", "Wet dreams.", "Synergistic management solutions.", "Growing a pair.", "Silence.", "An M16 assault rifle.", "Poopy diapers.", "A live studio audience.", "The Great Depression.", "A spastic nerd.", "Rush Limbaugh's soft, shitty body.", "Tickling Sean Hannity, even after he tells you to stop.", "Stalin.", "Brown people.", "Rehab.", "Capturing Newt Gingrich and forcing him to dance in a monkey suit.", "Battlefield amputations.", "An uppercut.", "Shiny objects.", "An ugly face.", "Menstrual rage.", "A bitch slap.", "One trillion dollars.", "Chunks of dead prostitute.", "The entire Mormon Tabernacle Choir.", "The female orgasm.", "Extremely tight pants.", "The Boy Scouts of America.", "Stormtroopers.", "Throwing a virgin into a volcano.", "Getting in her pants, politely.", "Gladiatorial combat.", "Good grammar.", "Hipsters.", "Gandalf.", "Genetically engineered super-soldiers.", "George Clooney's musk.", "Getting abducted by Peter Pan.", "Eating an albino.", "Enormous Scandinavian women.", "Fabricating statistics.", "Finding a skeleton.", "Suicidal thoughts.", "Dancing with a broom.", "Deflowering the princess.", "Dorito breath.", "One thousand Slim Jims.", "My machete.", "Overpowering your father.", "Ominous background music.", "Media coverage.", "Making the penises kiss.", "Moral ambiguity.", "Medieval Times&reg; Dinner &amp; Tournament.", "Mad hacky-sack skills.", "Just the tip.", "Literally eating shit.", "Leveling up.", "Insatiable bloodlust.", "Historical revisionism.", "Jean-Claude Van Damme.", "Jafar.", "The boners of the elderly.", "The economy.", "Statistically validated stereotypes.", "Sudden Poop Explosion Disease.", "Slow motion.", "Space muffins.", "Sexual humiliation.", "Sexy Siamese twins.", "Santa Claus.", "Scrotum tickling.", "Ripping into a man's chest and pulling out his still-beating heart.", "Ryan Gosling riding in on a white horse.", "Quivering jowls.", "Revenge fucking.", "Pistol-whipping a hostage.", "Quiche.", "Zeus's sexual appetites.", "Words, words, words.", "Tripping balls.", "Being a busy adult with many important things to do.", "The four arms of Vishnu.", "The shambling corpse of Larry King.", "The hiccups.", "The harsh light of day.", "The Gulags.", "The Fanta&reg; girls.", "A big black dick.", "A beached whale.", "A low standard of living.", "A nuanced critique.", "A bloody pacifier.", "A crappy little hand.", "Shaft.", "Being a dinosaur.", "Beating your wives.", "Neil Patrick Harris.", "Coughing into a vagina.", "Carnies.", "Nubile slave boys.", "Bosnian chicken farmers.", "A web of lies.", "A rival dojo.", "A passionate Latino lover.", "Panty raids.", "Appreciative snapping.", "Apologizing.", "Clams.", "A woman scorned.", "Being awesome at sex.", "Spring break!", "Another shot of morphine.", "Dining with cardboard cutouts of the cast of \"Friends.\"", "A soulful rendition of \"Ol' Man River.\"", "Making a friend.", "A sweaty, panting leather daddy.", "Intimacy problems.", "The new Radiohead album.", "Pretty Pretty Princess Dress-Up Board Game&reg;.", "A man in yoga pants with a ponytail and feather earrings.", "An army of skeletons.", "A squadron of moles wearing aviator goggles.", "Beefin' over turf.", "The Google.", "Bullshit.", "A sweet spaceship.", "A 55-gallon drum of lube.", "Special musical guest, Cher.", "The human body.", "Mild autism.", "Nunchuck moves.", "Whipping a disobedient slave.", "An ether-soaked rag.", "Oncoming traffic.", "A dollop of sour cream.", "A slightly shittier parallel universe.", "My first kill.", "Boris the Soviet Love Hammer.", "The grey nutrient broth that sustains Mitt Romney.", "Tiny nipples.", "Power.", "Death by Steven Seagal.", "A Burmese tiger pit.", "Basic human decency.", "Grandpa's ashes.", "One Ring to rule them all.", "The day the birds attacked.", "Fetal alcohol syndrome.", "Graphic violence, adult language, and some sexual content.", "A bigger, blacker dick.", "The mere concept of Applebee's&reg;.", "A sad fat dragon with no friends.", "A pi&ntilde;ata full of scorpions.", "Existing.", "Hillary Clinton's death stare.", "Catastrophic urethral trauma.", "Double penetration.", "Daddy's belt.", "Swiftly achieving orgasm.", "Mooing.", "Rising from the grave.", "Subduing a grizzly bear and making her your wife.", "Some really fucked-up shit.", "Weapons-grade plutonium.", "All of this blood.", "Scrotal frostbite.", "Taking a man's eyes and balls out and putting his eyes where his balls go and then his balls in the eye holes.", "The mixing of the races.", "Pumping out a baby every nine months.", "Tongue.", "Loki, the trickster god.", "Whining like a little bitch.", "Wearing an octopus for a hat.", "An unhinged ferris wheel rolling toward the sea.", "Finding Waldo.", "Upgrading homeless people to mobile hotspots.", "A magic hippie love cloud.", "Fuck Mountain.", "Living in a trashcan.", "The corporations.", "Getting hilariously gang-banged by the Blue Man Group.", "Jeff Goldblum.", "Survivor's guilt.", "Me.", "All my friends dying.", "Shutting the fuck up.", "An ass disaster.", "Some kind of bird-man.", "The entire Internet.", "Going around punching people.", "A boo-boo.", "Indescribable loneliness.", "Having sex on top of a pizza.", "Chugging a lava lamp.", "Warm, velvety muppet sex.", "Running naked through a mall, pissing and shitting everywhere.", "Nothing.", "Samuel L. Jackson.", "Self-flagellation.", "The systematic destruction of an entire people and their way of life.", "The Quesadilla Explosion Salad&trade; from Chili's&reg;.", "Reverse cowgirl.", "Vietnam flashbacks.", "Actually getting shot, for real.", "Not having sex.", "Cock.", "Dying alone and in pain.", "A cop who is also a dog.", "The way white people is.", "Gay aliens.", "The primal, ball-slapping sex your parents are having right now.", "A cat video so cute that your eyes roll back and your spine slides out of your anus.", "A lamprey swimming up the toilet and latching onto your taint.", "Slapping a racist old lady.", "A black male in his early 20s, last seen wearing a hoodie.", "Jumping out at people.", "Three months in the hole.", "Blood farts.", "The Land of Chocolate.", "A botched circumcision.", "My manservant, Claude.", "Vomiting mid-blowjob.", "Letting everyone down.", "Having shotguns for legs.", "Bill Clinton, naked on a bearskin rug with a saxophone.", "Mufasa's death scene.", "The Harlem Globetrotters.", "Demonic possession.", "Fisting.", "The thin veneer of situational causality that underlies porn.", "Girls that always be textin'.", "Blowing some dudes in an alley.", "A spontaneous conga line.", "A vagina that leads to another dimension.", "Disco fever.", "Getting your dick stuck in a Chinese finger trap with another dick.", "Drinking ten 5-hour ENERGYs&reg; to get fifty continuous hours of energy.", "Sneezing, farting, and coming at the same time.", "Some douche with an acoustic guitar.", "Spending lots of money.", "Putting an entire peanut butter and jelly sandwich into the VCR.", "An unstoppable wave of fire ants.", "A greased-up Matthew McConaughey.", "Flying robots that kill people.", "Unlimited soup, salad, and breadsticks.", "Crying into the pages of Sylvia Plath.", "The moist, demanding chasm of his mouth.", "Filling every orifice with butterscotch pudding.", "An all-midget production of Shakespeare's <i>Richard III<\/i>.", "Screaming like a maniac.", "Not contributing to society in any meaningful way.", "A pile of squirming bodies.", "Buying the right pants to be cool.", "Roland the Farter, flatulist to the king.", "That ass.", "A surprising amount of hair.", "Eating Tom Selleck's mustache to gain his powers.", "Velcro&trade;.", "A PowerPoint presentation.", "Crazy opium eyes.", "10 Incredible Facts About the Anus.", "An interracial handshake.", "Moderate-to-severe joint pain.", "Finally finishing off the Indians.", "Sugar madness.", "Actual mutants with medical conditions and no superpowers.", "The secret formula for ultimate female satisfaction.", "The complex geopolitical quagmire that is the Middle East.", "Fucking a corpse back to life.", "Neil Diamond's Greatest Hits.", "Calculating every mannerism so as not to suggest homosexuality.", "Whatever a McRib&reg; is made of.", "No clothes on, penis in vagina.", "All the single ladies.", "Whispering all sexy.", "How awesome I am.", "Ass to mouth.", "Smoking crack, for instance.", "Falling into the toilet.", "A dance move that's just sex.", "The size of my penis.", "Some sort of Asian.", "A hopeless amount of spiders.", "Party Mexicans.", "Drinking responsibly.", "The safe word.", "Angelheaded hipsters burning for the ancient heavenly connection to the starry dynamo in the machinery of night.", "Bouncing up and down.", "Jizz.", "Ambiguous sarcasm.", "A shiny rock that proves I love you.", "Dem titties.", "My worthless son.", "Exploding pigeons.", "A Ugandan warlord.", "My sex dungeon.", "A kiss on the lips.", "Child Protective Services.", "A Native American who solves crimes by going into the spirit world.", "Doo-doo.", "The peaceful and nonthreatening rise of China.", "Sports.", "A fart.", "Unquestioning obedience.", "Three consecutive seconds of happiness.", "Grammar nazis who are also regular Nazis.", "Snorting coke off a clown's boner.", "Africa.", "Depression.", "A horse with no legs.", "The euphoric rush of strangling a drifter.", "Khakis.", "Interspecies marriage.", "A gender identity that can only be conveyed through slam poetry.", "Almost giving money to a homeless person.", "Stuff a child's face with Fun Dip&reg; until he starts having fun.", "What Jesus would do.", "A for-real lizard that spits blood from its eyes.", "Blackula.", "The tiniest shred of evidence that God is real.", "My dad's dumb fucking face.", "Prince Ali,<br>fabulous he,<br>Ali Ababwa.", "A manhole.", "A sex goblin with a carnival penis.", "A bunch of idiots playing a card game instead of interacting like normal humans.", "A sex comet from Neptune that plunges the Earth into eternal sexiness.", "Sharks with legs.", "Injecting speed into one arm and horse tranquilizer into the other.", "Lots and lots of abortions.", "Seeing things from Hitler's perspective", "Too much cocaine.", "Doing the right stuff to her nipples.", "Giant sperm from outer space.", "Oil!", "Ennui.", "A powered exoskeleton.", "A disappointing salad.", "Mom's new boyfriend.", "Unrelenting genital punishment.", "Denzel.", "The swim team, all at once.", "The eight gay warlocks who dictate the rules of fashion.", "Being nine years old.", "The unbelievable world of mushrooms.", "The Abercrombie &amp; Fitch lifestyle.", "Vegetarian options.", "My first period.", "Having been dead for a while.", "Backwards knees.", "Being paralyzed from the neck down.", "Seeing my village burned and my family slaughtered before my eyes.", "A zero-risk way to make $2,000 from home.", "A crazy little thing called love.", "Ancient Athenian boy-fucking", "Out-of-this-world bazongas.", "The ghost of Marlon Brando.", "The basic suffering that pervades all of existence.", "Being worshipped as the one true God.", "Figuring out how to have sex with a dolphin.", "All these decorative pillows.", "A mouthful of potato salad.", "Russian super-tuberculosis.", "A reason not to commit suicide.", "Going to a high school reunion on ketamine.", "The passage of time.", "Child support payments.", "Changing a person's mind with logic and facts.", "My boyfriend's stupid penis.", "The tiger that killed my father.", "Genghis Khan's DNA.", "Boring vaginal sex.", "40 acres and a mule.", "A whole new kind of porn.", "Slowly easing down onto a cucumber.", "Wearing glasses and sounding smart.", "AIDS monkeys.", "A team of lawyers.", "Getting drive-by shot.", "Not believing in giraffes.", "Anal fissures like you wouldn't believe.", "A giant powdery manbaby.", "Cutting off a flamingo's legs with garden shears.", "P.F. Chang himself.", "An uninterrupted history of imperialism and exploitation.", "A one-way ticket to Gary, Indiana.", "Daddy's credit card.", "September 11th, 2001.", "An unforgettable quincea&ntilde;era.", "Deez nuts.", "Social justice warriors with flamethrowers of compassion.", "Some shit-hot guitar licks.", "Butt stuff.", "Blackface.", "Blowjobs for everyone.", "Getting eaten alive by Guy Fieri.", "Western standards of beauty.", "Ejaculating live bees and the bees are angry.", "My dead son's baseball glove.", "Getting caught by the police and going to jail.", "A face full of horse cum.", "Free ice cream, yo.", "The white half of Barack Obama.", "The black half of Barack Obama.", "An inability to form meaningful relationships.", "A bass drop so huge it tears the starry vault asunder to reveal the face of God.", "Growing up chained to a radiator in perpetual darkness.", "Shitting all over the floor like a bad, bad girl.", "A buttload of candy.", "Sucking all the milk out of a yak.", "Bullets.", "A man who is so cool that he rides on a motorcycle.", "Sudden penis loss.", "Getting all offended.", "Crying and shitting and eating spaghetti.", "One unforgettable night of passion.", "Being popular and good at sports.", "Filling a man's anus with concrete.", "Two whales fucking the shit out of eachother.", "Cool, releatable cancer teens.", "The amount of gay I am.", "A possible Muslim.", "Unsheathing my massive horse cock.", "A bowl of gourds.", "The male gaze.", "The power of the Dark Side.", "Ripping a dog in half.", "A constant need for validation.", "Meaningless sex.", "Such a big boy.", "Throwing stones at a man until he dies.", "Cancer.", "Like a million alligators.", "Eating together like a god damn family for once.", "Cute boys.", "Pussy.", "Being a terrible mother.", "Never having sex again.", "A pizza guy who fucked up.", "A whole lotta woman.", "The all-new Nissan Pathfinder with 0.9% APR financing!", "A peyote-fueled vision quest.", "Kale.", "Breastfeeding a ten year old.", "Crippling social anxiety.", "Immortality cream.", "Texas.", "Teaching a girl how to handjob the penis.", "A turd.", "Shapes and colors.", "Whatever you wish, mother.", "The haunting stare of an Iraqi child.", "Robots who just want to party.", "A self-microwaving burrito.", "Forgetting grandma's first name.", "Our new Buffalo Chicken Dippers&reg;!", "Treasures beyond your wildest dreams.", "Getting shot out of a cannon.", "The sweet song of sword against and the braying of mighty war beasts.", "Walking into a glass door.", "The color \"puce\".", "Every ounce of charisma left in Mick Jagger's tired body.", "The eighth graders.", "Setting my balls on fire and cartwheeling to Ohio.", "The dentist.", "Gwyneth Paltrow's opinions.", "Turning the rivers red with the blood of infidels.", "Rabies.", "Important news about Taylor Swift.", "Ejaculating inside another man's wife.", "Owls, the perfect predator.", "Being John Malkovich.", "Bathing in moonsblood and dancing around the ancient oak.", "An oppressed people with a vibrant culture.", "An overwhelming variety of cheeses.", "Reading the entire End-User License Agreement.", "Morpheus.", "Peeing into a girl's butt to make a baby.", "Generally having no idea of what's going on.", "No longer finding any Cards Against Humanity card funny.", "A supermassive black hole.", "Reconciling quantum theory with general relativity.", "Electroejaculating a capuchin monkey.", "Insufficient serotonin.", "Evolving a labyrinthine vagina.", "Getting really worried about global warming for a few seconds.", "Infinity.", "Oxytocin release via manual stimulation of the nipples.", "Uranus.", "Being knowledgeable in a narrow domain that nobody understands or cares about.", "Achieving reproductive success.", "Slowly evaporating.", "The quiet majesty of the sea turtle.", "A 0.7 waist-to-hip ratio.", "Fun and interesting facts about rocks.", "Photosynthesis.", "Developing secondary sex characteristics.", "Failing the Turing test.", "Explosive decompression.", "Driving into a tornado to learn about tornadoes.", "David Attenborough watching us mate.", "3.7 billion years of evolution.", "The Sun engulfing the Earth.", "My dick in your mouth.", "Corn.", "Carribbean Jesus.", "Super yoga.", "Ladles.", "Ejaculating a pound of tinsel.", "A heart that is two sizes too small and that therefore cannot pump an adequate amount of blood.", "The John D. and Catherine T. MacArthur Foundation.", "Dividing by zero.", "Playing an ocarina to summon Ultra-Congress from the sea.", "Asshole pomegranates that are hard to eat.", "Becoming so rich that you shed your body and turn to vapor.", "Faking a jellyfish sting so someone will pee on you.", "A sexy naked interactive theater thing.", "A giant squid in a wedding gown.", "Crawling into a vagina.", "Poutine.", "Newfies.", "The Official Languages Act. La Loi sur les langues officielles.", "Terry Fox's prosthetic leg.", "The FLQ.", "Canada: America's Hat.", "Don Cherry's wardrobe.", "Burning down the White House.", "Heritage minutes.", "Homo milk.", "Naked News.", "Mr. Dressup.", "Syrupy sex with a maple tree.", "Being Canadian.", "Snotsicles.", "The Famous Five.", "Schmirler the Curler.", "Stephen Harper.", "A Molson muscle.", "The Royal Canadian Mounted Police.", "An icy handjob from an Edmonton hooker.", "Making 77 cents on the dollar (unless you're Latina).", "Inspirational Dove chocolate wrappers.", "Masturbating to Ty Pennington.", "Pretending you'll wear that bridesmaid dress again.", "Mansplaining.", "Beyonce thinkpieces.", "Gabby Giffords' physical therapy.", "Female genital mutilation.", "When the tampon's too low and you feel it with every step.", "A joke too funny for women to understand.", "Meryl Streep selfies.", "Hillary bitch-slapping Bill with a frozen tuna.", "The Bechdel Test.", "Staph infections from dirty nail salons.", "Stalking wedding photos on Facebook, weeping softly.", "Emma Goldman burning the whole motherfucker down.", "Douches that smell like rain.", "Rosa Parks' back seat.", "Engagement photos on train tracks.", "Choking on the ashes of Gloria Steinem's bras.", "The cold, hard truth that no lesbian has ever scissored.", "Doing your kegels at work.", "Misandry.", "Abortion Barbie.", "Telling a street harasser \"You know what? I <i>will<\/i> blow you.\"", "Peggy Olson's cutthroat ambitions.", "A quickie with Rachel Maddow in the green room.", "Force-feeding Sheryl Sandberg the pages of Lean In, one by one.", "Emma Watson, Emma Stone, EMMA THOMPSON BITCHES.", "Dumpsters overflowing with whimisical save-the-date magnets.", "The torture chamber where Kathryn Bigelow keeps James Cameron.", "A detailed vajazzling of Van Goh's Starry Night.", "STOP MAKING ME PRETEND TO CARE ABOUT YOUR WEDDING PINTEREST DARLA.", "The Chub Rub.", "Chin hairs you pretend you don't have.", "A strongly worded letter to Netflix demanding the addition of \"The Good Wife\".", "Doubling up on sports bras.", "When a dog smells your crotch and you know exactly why.", "Malala's gunshot wounds.", "Lactating when a stranger's baby cries on the train.", "A new cookbook by Sylvia Plath.", "Crying in the fitting room during bikini season.", "Eating the entire bag.", "Scalding hot wax right there on your labia.", "The G-Spot, the Y-spot, the other spot you made up to confuse your partner.", "The Golden Girls' never-ending supply of frozen cheesecake.", "Underboob swet like rancid milk.", "Wondering whether your girl crush on Hermione constitutes pedophilia.", "Taking a giant dump on the 18th green at the Augusta National Golf Club.", "A brown smudge equally likely to be period blood or chocolate.", "A hand-crocheted Diva Cup case from Etsy.", "Your gigantic crush on Jenna Lyons.", "A one-way ticket to Steubenville.", "A bodice-ripping 4-way with Alexander Skarsgard, Ian Somerhalder, and David Boreanaz.", "Finger banging Michelle Rodriguez.", "Princess Aurora maniacally devouring the still-beating heart of Maleficent.", "A tear stained copy of Reviving Ophelia.", "A misogynist dystopia set in a not-too-distant WAIT A MINUTE.", "Dying your hair red like Angela Chase.", "Daenerys Targaryen's fire-breathing vajayjay.", "Resentfully clicking like on your boss's vacation photos.", "Calmly informing your date that you understand the infield fly rule better than he does.", "Tina Fey and Amy Poehler making out on a pile of Bitch magazines.", "Only shaving up to the knee.", "Meredith Grey's slut phase.", "Shameful childhood memories of envying the wheelchair girl who got all the attention.", "Sort of wishing the baby on the plane would die.", "Tenderly dominating Uncle Jesse from behind.", "Stumbling on David Wright performing as Judy Garland in the East Village.", "Urinating on yourself to prevent an assault.", "When a FOX News anchor causally references 'ebonics'.", "Getting DPed by the Property Brothers on a custom granite countertop.", "Being compared to a Cathy Cartoon on Metafilter.", "Kim Kardashian's placenta banh mi.", "Asking Gilbert Gottfried to do the Iago voice during sex.", "Watching Bethenny Frankel struggle for life in a churning sea of pre-mixed SkinnyGirl cocktails.", "An alternate version of the Washington Monument that looks kind of like a vagina.", "Telling Pacey your innermost secrets in a canoe beneath the Capeside stars.", "The blue liquid from tampon commercials.", "Patti Stanger's line of jewelry.", "#solidarityisforwhitewomen.", "A candlelight vigil for Nicole Brown Smith.", "Sexual fantasies involving Mindy Lahiri and a sumptuous coffeecake.", "Tweeting Cory Booker about that guy walking behind you.", "A gender-neutral, owl-themed baby announcement.", "Being the only woman at the office-mandated sexual harassment training.", "Asking Larry Summers increasingly difficult mathematical questions until Bar and Mat Mitzvahs are considered equally important.", "Cramming Vladimir Putin full of Activia until he poops out Russia's homophobia.", "A breath of fresh air.", "A great big floppy donkey dick.", "A pyramid scheme.", "A school bus surrounded by cop cars.", "A short walk in the desert with shovels.", "All the boys staring at your chest.", "An amorous stallion.", "Being so wet it just slides out of you.", "Being tarred and feathered.", "Catching 'em all.", "Chained to the bed and whipped to orgasmic bliss by a leather-clad woman.", "Child-bearing hips.", "Defenestration.", "Dungeons and\/or dragons.", "Ecco the Dolphin.", "George Washington riding on a giant eagle.", "Getting abducted and probed by aliens.", "Going viral on YouTube.", "Gushing.", "Making the baby Jesus cry.", "More than you can chew.", "Napalm.", "Pancake bitches.", "Playing God with the power of lightning.", "Playing tonsil-hockey.", "Racing cheese wheels downhill.", "Riding the bomb.", "Settling arguments with dance-offs.", "Sheer spite.", "Sinister laughter.", "SS Girls.", "Stealing your sister's underwear.", "Stroking a cat the wrong way.", "Sucking and blowing.", "The bullet with your name on it.", "The entire rest of eternity, spent in fucking Bruges.", "The oceans rising to reclaim the land.", "A cocained-fuelled sex orgy heart attack.", "A cocktail umbrella ", "A murder\/suicide pact.", "A squirming mass of kittens.", "An angry mob with torches and pitchforks.", "Biting my girlfriend like a vampire during sex.", "Dropping your pants and saluting.", "Frankenstein's Monster", "Getting a blowjob in a theater.", "Going full retard.", "Going slob-slob-slob all over that knob.", "Leaking implants.", "Low-flying planes.", "Monkies flinging their own shit.", "My robot duplicate.", "Other people's children.", "People who can't take a joke. Seriously.", "Popping a boner during Sex Ed class.", "Projectile vomiting.", "Pulling down panties with your teeth.", "Saying ", "Shedding skin like a snake.", "Shooting Valley Girls for like, saying like all the time. Really.", "Slow seductive tentacle rape.", "Talking like a pirate, y'arr!", "Tenderly kissing a unicorn's horn.", "That bastard Jesus!", "The last shreads of dignity.", "The power of friendship.", "This card intentionally left blank.", "Throwing water on a braless woman in a white t-shirt", "Upskirts.", "Wasting all your money on hookers and booze.", "Winning.", "A foot fetish.", "A powerful gag reflex.", "A tight, Asian pussy.", "Extraordinary Rendition.", "Forgetting the safety word.", "Greeting Christmas carollers naked.", "Handcuffs, without the key.", "Having a drill for a penis.", "Hot Jailbait Ass.", "Liposuction gone horrible wrong.", "My harem of scantily clad women.", "Nazi Zombie Robot Ninjas.", "Redneck gypsies.", "Scissoring.", "A guy and two robots who won't shut up.", "A shotgun wedding.", "Anne Frank's diary", "Autoerotic asphyxiation.", "Blow Up Bianca the Latex Lovedoll.", "Endlessly tumbling down an up escalator.", "Fun with nuns.", "Getting it all over the walls.", "Holiday Dinner by Jack Daniels.", "Nailgun fights.", "Teaching the bitch a lesson.", "Nazi super science.", "Making a human centipede.", "Mr. Fancy Pants.", "TotalBiscuit's top hat.", "Skullcrusher Mountain.", "Penn and Teller.", "An 8-ball.", "Rule 34.", "The smallest, whitest dick.", "Fruit Fuckers.", "Beer Pong.", "Using hot sauce as lube.", "Smooth jazz.", "Soviet Russia.", "Indiana Jones.", "Hentai.", "The Columbine Shooting.", "Mr. Hankey the Christmas Poo.", "Two midgets stacked up pretending to be one person.", "The Bible.", "Xyzzy playing around with gender roles.", "Four Loko.", "Mario brutally murdering Sonic.", "Sonic brutally murdering Mario.", "The world's tallest midget.", "The brown note.", "Tits.", "Rainbows and magic.", "Monkeys throwing shit.", "Hitler's mustache.", "ALL the things!", "Sheepskin Condoms.", "Hump Day.", "Yankee Stadium.", "Air Bud.", "Hordes of zombies.", "Donkey Shows.", "The DK Rap.", "Cranky Kong.", "The Eiffel Tower.", "Chicken and Waffles.", "Shag carpeting.", "Dirty hippies.", "Hey Arnold!", "The Oculus Rift.", "Banana Hammocks.", "Nessie.", "Princess Peach's Cake.", "Shitting on the White House lawn.", "Mountain Dew&reg; Baja Blast.", "The tears of a college student.", "Apples to Apples&reg;.", "Snorting Pixie Stix.", "Getting high on bath salts.", "The final circle of Hell.", "Poorly written Star Wars&reg; fan fiction.", "Borat's one piece.", "A 1971 Ford Pinto.", "LOOK AT THIS PHOTOGRAPH!", "Master Chief.", "The shitty remains of Taco Bell&reg;.", "Sponge baths.", "Walt Disney's frozen head.", "Hoola hoops.", "Canadian Kindness.", "A walrus with a beret.", "Speedrunning life.", "Demi Moore's bush.", "Eating 120 White Castle burgers&reg;.", "Blowing the President.", "Indentured servants.", "Sex in your mouth.", "Bong hits for Jesus.", "Quiznos&reg;.", "Scotsmen marrying their sheep.", "The truffle shuffle.", "Nurse Joy.", "Barney's rape dungeon.", "Tickle Me Elmo.", "Viking Metal.", "Teh Urn.", "Spanish soap operas.", "Made-for-TV movies.", "Terabytes of horse porn.", "The hero of time.", "Ringworm.", "Rich, chocolatey Ovaltine&reg;.", "Abusive fathers.", "Hungry Hungry Hippos.", "Canadian tuxedo.", "Insane Clown Posse.", "Nu Metal.", "The Wiggles.", "Blue's Clues.", "Kaizo Mario.", "FrankerZ.", "Spontaneous Combustion.", "Kappa.", "Blowing your hand off with a firework.", "Duke Nukem Forever.", "Mating season.", "The Ouya.", "Jew-Fros.", "Catdog.", "Hipsters on their iPhones at Starbucks.", "A boat load of cocaine.", "Song of Storms.", "Gerudo Valley.", "A giant purple dildo sword.", "Bill Gates pissing on Steve Jobs's grave.", "Outsourcing jobs to India.", "The Mushroom Kingdom.", "The Hokey Pokey.", "Hooters.", "Blue Waffles.", "Lemon grenades.", "360 no scopes.", "DeviantArt.", "Sonic the Hedgehog.", "StarFox.", "PetSmart.", "A tub of Vaseline.", "Sex with strangers.", "That one episode of CSI.", "Cloaca.", "Lovingly rendered dragon anus.", "Drawing furry porn.", "A hermaphrodite snow leopard.", "An oversized clitoris that acts as a functional penis.", "The texture and color of raw meat.", "Applying your obscure, unrealistic fetishes to 90's cartoon characters.", "Really, really liking Disney's Robin Hood.", "Taking the knot.", "Heated debates about human genitalia versus animal genitalia.", "Otherkin.", "Chakats.", "Fursecution.", "Horses.", "Intimacy with the family dog.", "A furpile.", "The stench of half a dozen unwashed bronies.", "Bowser's sweaty balls.", "Krystal, the fox.", "Confusing feelings about cartoon characters.", "Babyfurs.", "Uncomfortably attractive animals.", "Stretching your anus in preparation for horse cock.", "A hermaphrodite foxtaur.", "Pounced.org.", "A large, flared Chance.", "Fursuiters at anime conventions.", "Embarrassing craigslist ads.", "An apartment full of internet-obsessed losers.", "People who cosplay at furry conventions.", "A semen-stained fursuit.", "Fursuit adventures.", "That one straight guy at the party.", "Fake furry girls.", "Adoptables with visible genitalia.", "My tailhole.", "Catching STDs at conventions.", "An embarrassingly long F-List profile.", "Dragon dildos.", "Non-consensual sex with Zaush.", "Adam Wan.", "Surprise hermaphrodites.", "A sassy lioness.", "Offensively stereotyped African animals.", "Poodles with afros.", "A sexually frustrated griffon.", "A horny dragon.", "Dragoneer.", "Discovering that it's never just a big vagina.", "That thing that gives your dick a knot IRL.", "All this lube.", "Endearing social ineptitude.", "Realizing that rimming is pretty cool.", "That time you let your dog go a little further than just sniffing your crotch.", "Drenching your fursuit in Febreeze.", "Really, truly heterosexual.", "Gay.", "Sitting on your face.", "Spending more money on commissions than food in a given week.", "Shitting on my face.", "Barking at strangers.", "Anal training.", "Discovering monster porn.", "A dick so big you have to give it a hugjob.", "The fine line between feral and outright bestiality.", "Bad Dragon&trade; cumlube.", "Piss.", "Leaving your orifices bloody and sore.", "Rubbing peanut butter on your genitals.", "Forgetting which set of fursuit paws you use for handjobs.", "A strategically placed hole.", "Smells.", "When \"blowing ten bucks\" makes you think of a long night with a bunch of deer.", "A lime Citra.", "A little bitch.", "An autistic knife fight.", "Sergals.", "About 16 ounces of horse semen.", "The noises red pandas make during sex.", "Oral knotting.", "Dog cum.", "Anatomically incorrect genitalia.", "Transformation porn.", "Belly rubs leading to awkward boners.", "When you catch yourself glancing at the crotches of animated characters.", "Lifting your tail.", "Scritches.", "Bad decisions.", "Experimenting with fisting.", "FUCK YOU, I'M A DRAGON.", "Tumbles, the Stair Dragon.", "Furry Weekend Atlanta.", "Further Confusion.", "AnthroCon.", "Literally a bucket of semen.", "Sexual interest in pretty much anything with a hole.", "Attraction to pretty much anything with a penis.", "Animal genitalia.", "Motherfucking wolves.", "Christian furries.", "Barbed penises.", "Two knots.", "A really attractive wolf.", "A slutty gay fox.", "A surprisingly attractive anteater.", "Natascha, the anthro husky.", "Nipple buffet.", "Tail-sex.", "Mary, the anthro mare.", "Masturbating, with claws.", "Pawing-off.", "Furry porn, shamelessly taped to the walls.", "Grabby-paws.", "Monsters with bedroom eyes.", "Accurate avian anatomy.", "Impure thoughts about Kobolds.", "Erotic roleplay.", "Furries in heat.", "Fantasizing about sex with just about every monster you encounter in your video game.", "Crotch-tits.", "The tailstar tango.", "Your Character Here.", "CrusaderCat.", "Jerking off on an unconscious friend's feet.", "Puns involving the word \"knot.\"", "A prehensile penis.", "Cockvore.", "A notebook full of embarrassing niche porn sketches.", "Being able to recognize your friends by the scent of their asses.", "Paws.", "Big cute paws.", "Bear tits.", "The incredibly satisfying sound it makes when you pull it out.", "Anal sex you didn't know you wanted.", "The premise of every furry comic ever.", "Becoming a veterinarian for all the wrong reasons.", "Taking special interest in nature documentaries.", "A very steampunk rat.", "Canine superiority.", "Oviposition.", "Flares.", "Overcompensating with a huge horse penis.", "A fedora enthusiast.", "A tongue-beast.", "Frisky tentacles.", "Sex with Pok&eacute;mon.", "Making out with dogs.", "YouTube videos of horse breeding.", "Pissing on your significant other to show ownership.", "Dogs wearing panties.", "Monster boys in lingerie.", "Power bottoms.", "Sheath licking.", "Microwaving diapers.", "Being \"prison gay.\"", "Sexy the Cat.", "Adorable dog people.", "HELLO FURRIEND, HOWL ARE YOU DOING.", "Species stereotypes.", "Horns and hooves.", "Convention sluts.", "Being really, really into monsters.", "A spider furry who isn't even into bondage.", "No males, no herms, no cuntboys, no shemales, no trannys, no furries, no aliens, no vampires, and no werewolves. ONLY STRAIGHT OR BI HUMAN FEMALES.", "Sexual arousal from children's cartoons.", "SecondLife.", "Hyper-endowed squirrels.", "The Gay Yiffy Club.", "Getting feathers stuck in your teeth.", "Getting fur stuck in your teeth.", "Ignoring a person's faults because their character is hot."],
  "Base": {
    "name": "Base Set",
    "black": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89],
    "white": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 258, 259, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277, 278, 279, 280, 281, 282, 283, 284, 285, 286, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296, 297, 298, 299, 300, 301, 302, 303, 304, 305, 306, 307, 308, 309, 310, 311, 312, 313, 314, 315, 316, 317, 318, 319, 320, 321, 322, 323, 324, 325, 326, 327, 328, 329, 330, 331, 332, 333, 334, 335, 336, 337, 338, 339, 340, 341, 342, 343, 344, 345, 346, 347, 348, 349, 350, 351, 352, 353, 354, 355, 356, 357, 358, 359, 360, 361, 362, 363, 364, 365, 366, 367, 368, 369, 370, 371, 372, 373, 374, 375, 376, 377, 378, 379, 380, 381, 382, 383, 384, 385, 386, 387, 388, 389, 390, 391, 392, 393, 394, 395, 396, 397, 398, 399, 400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417, 418, 419, 420, 421, 422, 423, 424, 425, 426, 427, 428, 429, 430, 431, 432, 433, 434, 435, 436, 437, 438, 439, 440, 441, 442, 443, 444, 445, 446, 447, 448, 449, 450, 451, 452, 453, 454, 455, 456, 457, 458, 459]
  },
  "CAHe1": {
    "name": "The First Expansion",
    "black": [90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109],
    "white": [460, 461, 462, 463, 464, 465, 466, 467, 468, 469, 470, 471, 472, 473, 474, 475, 476, 477, 478, 479, 480, 481, 482, 483, 484, 485, 486, 487, 488, 489, 490, 491, 492, 493, 494, 495, 496, 497, 498, 499, 500, 501, 502, 503, 504, 505, 506, 507, 508, 509, 510, 511, 512, 513, 514, 515, 516, 517, 518, 519, 520, 521, 522, 523, 524, 525, 526, 527, 528, 529, 530, 531, 532, 533, 534, 535, 536, 537, 538, 539],
    "icon": 1
  },
  "CAHe2": {
    "name": "The Second Expansion",
    "black": [110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134],
    "white": [540, 541, 542, 543, 544, 545, 546, 547, 548, 549, 550, 551, 552, 553, 554, 555, 556, 557, 558, 559, 560, 561, 562, 563, 564, 565, 566, 567, 568, 569, 570, 571, 572, 573, 574, 575, 576, 577, 578, 579, 580, 581, 582, 583, 584, 585, 586, 587, 588, 589, 590, 591, 592, 593, 594, 595, 596, 597, 598, 599, 600, 601, 602, 603, 604, 605, 606, 607, 608, 609, 610, 611, 612, 613, 614],
    "icon": 2
  },
  "CAHe3": {
    "name": "The Third Expansion",
    "black": [135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159],
    "white": [615, 616, 617, 618, 619, 620, 621, 622, 623, 624, 625, 626, 627, 628, 629, 630, 631, 632, 633, 634, 635, 636, 637, 638, 639, 640, 641, 642, 643, 644, 645, 646, 647, 648, 649, 650, 651, 652, 653, 654, 655, 656, 657, 658, 659, 660, 661, 662, 663, 664, 665, 666, 667, 668, 669, 670, 671, 672, 673, 674, 675, 676, 677, 678, 679, 680, 681, 682, 683, 684, 685, 686, 687, 688, 689],
    "icon": 3
  },
  "CAHe4": {
    "name": "The Fourth Expansion",
    "black": [160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189],
    "white": [690, 691, 692, 693, 694, 695, 696, 697, 698, 699, 700, 701, 702, 703, 704, 705, 706, 707, 708, 709, 710, 711, 712, 713, 714, 715, 716, 717, 718, 719, 720, 721, 722, 723, 724, 725, 726, 727, 728, 729, 730, 731, 732, 733, 734, 735, 736, 737, 738, 739, 740, 741, 742, 743, 744, 745, 746, 747, 748, 749, 750, 751, 752, 753, 754, 755, 756, 757, 758, 759],
    "icon": 4
  },
  "CAHe5": {
    "name": "The Fifth Expansion",
    "black": [190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214],
    "white": [760, 761, 762, 763, 764, 765, 766, 767, 768, 769, 770, 771, 772, 773, 774, 775, 776, 777, 778, 779, 780, 781, 782, 783, 784, 785, 786, 787, 788, 789, 790, 791, 792, 793, 794, 795, 796, 797, 798, 799, 800, 801, 802, 803, 804, 805, 806, 807, 808, 809, 810, 811, 812, 813, 814, 815, 816, 817, 818, 819, 820, 821, 822, 823, 824, 825, 826, 827, 828, 829, 830, 831, 832, 833, 834],
    "icon": 5
  },
  "CAHe6": {
    "name": "The Sixth Expansion",
    "black": [215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239],
    "white": [835, 836, 837, 838, 839, 840, 841, 842, 843, 844, 845, 846, 847, 848, 849, 850, 851, 852, 853, 854, 855, 856, 857, 858, 859, 860, 861, 862, 863, 864, 865, 866, 867, 868, 869, 870, 871, 872, 873, 874, 875, 876, 877, 878, 879, 880, 881, 882, 883, 884, 885, 886, 887, 888, 889, 890, 891, 892, 893, 894, 895, 896, 897, 898, 899, 900, 901, 902, 903, 904, 905, 906, 907, 908, 909],
    "icon": 6
  },
  "science": {
    "name": "Science Pack",
    "black": [240, 241, 242, 243, 244, 245, 246],
    "white": [910, 911, 912, 913, 914, 915, 916, 917, 918, 919, 920, 921, 922, 923, 924, 925, 926, 927, 928, 929, 930, 931, 932],
    "icon": "flask"
  },
  "reject": {
    "name": "Reject Pack",
    "black": [247, 248, 249, 250, 251, 252, 253, 254],
    "white": [933, 934, 935, 936, 937, 938, 939, 940, 941, 942, 943, 944, 945, 946, 947, 948],
    "icon": "thumbs-down"
  },
  "Canadian": {
    "name": "Canadian",
    "black": [255, 256, 257, 258, 259],
    "white": [949, 950, 951, 952, 953, 954, 955, 956, 957, 958, 959, 960, 961, 962, 963, 964, 965, 966, 967, 968, 969],
    "icon": "leaf"
  },
  "c-ladies": {
    "name": "[C] Ladies Against Humanity",
    "black": [260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277, 278, 279, 280, 281, 282, 283, 284],
    "white": [970, 971, 972, 973, 974, 975, 976, 977, 978, 979, 980, 981, 982, 983, 984, 985, 986, 987, 988, 989, 990, 991, 992, 993, 994, 995, 996, 997, 998, 999, 1000, 1001, 1002, 1003, 1004, 1005, 1006, 1007, 1008, 1009, 1010, 1011, 1012, 1013, 1014, 1015, 997, 1016, 1017, 1018, 1019, 1020, 1021, 1022, 1023, 1024, 1025, 1026, 1027, 1028, 1029, 1030, 1031, 1032, 1033, 1034, 1035, 1036, 1037, 1038, 1039, 1040, 1041, 1042, 1043, 1044, 1045, 1046, 1047, 1048, 1049, 1050, 1051, 1052, 1053, 1054, 1055, 1056, 1057],
    "icon": "female"
  },
  "NSFH": {
    "name": "[C] Not Safe For Humanity",
    "black": [285, 286, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296, 297, 298, 299, 300, 301, 302, 303, 304, 305, 306, 307, 308, 309, 310, 311, 312, 313, 314, 315, 316],
    "white": [1058, 1059, 1060, 1061, 1062, 1063, 1064, 1065, 1066, 1067, 1068, 1069, 1070, 1071, 1072, 1073, 1074, 1075, 1076, 1077, 1078, 1079, 1080, 1081, 1082, 1083, 1084, 1085, 1086, 1087, 1088, 1089, 1090, 1091, 1092, 1093, 1094, 1095, 1096, 1097, 1098, 1099, 1100, 1101, 1102, 1103, 1104, 1105, 1106, 1107, 1108, 1109, 1110, 1111, 1112, 1113, 1114, 1115, 1116, 1117, 1118, 1119, 1120, 1121, 1122, 1123, 1124, 1125, 1126, 1127, 1128, 1129, 1130, 1131, 928, 1132, 1133, 1134, 1135, 1136, 1137, 1138, 1139, 1140, 1141, 1142, 1143, 1144, 1145, 1146, 1147, 1148, 1149, 1150, 1151, 1152, 1153, 1154, 1155],
    "icon": "exclamation-triangle"
  },
  "c-stupid": {
    "name": "[C] Ridiculously Stupid",
    "black": [317, 318, 319, 320, 321, 322, 323, 324, 325, 326, 327, 328, 329, 330, 331, 332, 333, 334, 335, 336, 337, 338, 339, 340, 341, 342, 343, 344, 345, 346, 347, 348, 349, 350, 351, 352, 353, 354, 355, 356, 357, 358, 359, 360, 361, 362, 363],
    "white": [1156, 1157, 1158, 1159, 1160, 1161, 1162, 1163, 1164, 1165, 1166, 1167, 1168, 1169, 1170, 1171, 1172, 1173, 1174, 1175, 1176, 1177, 1178, 1179, 1180, 1181, 1182, 1183, 1184, 1185, 1186, 1187, 1188, 1189, 1190, 1191, 1192, 1193, 1194, 1195, 1196, 1197, 1198, 1199, 1200, 1201, 1202, 1203, 1204, 1205, 1206, 1207, 1208, 1209, 1210, 1211, 1212, 1213, 1214, 1215, 1216, 1217, 1218, 1219, 1220, 1221, 1222, 1223, 1224, 1225, 1226, 1227, 1228, 1229, 1230, 1231, 1232, 1233, 1234, 1235, 1236, 1237, 1238, 1239, 1240, 1241, 1242, 1243, 1244, 1245, 1246, 1247, 1248, 1249, 1250, 1251, 1252, 1253, 1254, 1255, 1256, 1257, 1258, 1259, 1260, 1261, 1262, 1263, 1264, 1265, 1266, 1267, 1268, 1269, 1270],
    "icon": "lemon-o"
  },
  "c-sodomydog": {
    "name": "[C] Sodomy Dog's Furry Pack",
    "black": [364, 365, 366, 367, 368, 369, 370, 371, 372, 373, 374, 375, 376, 377, 378, 379, 380, 381, 382, 383, 384, 385, 386, 387, 388, 389, 390, 391, 392, 393, 394, 395, 396, 397, 398, 399, 400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417, 418, 419, 420, 421, 422, 423, 424, 425, 426, 427, 428, 429, 430, 431, 432, 433, 434, 435, 436, 437, 438, 439, 440, 441, 442, 443, 444, 445, 446, 447, 448, 449, 450, 451, 452, 453, 454, 455, 456, 457, 458, 459, 460, 461, 462, 463, 464, 465, 466, 467, 468],
    "white": [1271, 1272, 1273, 1274, 1275, 1276, 1277, 1278, 1279, 1280, 1281, 1282, 1283, 1284, 1285, 1286, 1287, 1288, 1289, 1290, 1291, 1292, 1293, 1294, 1295, 1296, 1297, 1298, 1299, 1300, 1301, 1302, 1303, 1304, 1305, 1306, 1307, 1308, 1309, 1310, 1311, 1312, 1313, 1314, 1315, 1316, 1317, 1318, 1319, 1320, 1321, 1322, 1323, 1324, 1325, 1326, 1327, 1328, 1329, 1330, 1331, 1332, 1333, 1334, 1335, 1336, 1337, 1338, 1339, 1340, 1341, 1342, 1343, 1344, 1345, 1346, 1347, 1348, 1349, 1350, 1351, 1352, 1353, 1354, 1355, 1356, 1357, 1358, 1359, 1360, 1361, 1362, 1363, 1364, 1365, 1366, 1367, 1368, 1369, 1370, 1371, 1372, 1373, 1374, 1375, 1376, 1377, 1378, 1379, 1380, 1381, 1382, 1383, 1384, 1385, 1386, 1387, 1388, 1389, 1390, 1391, 1392, 1393, 1394, 1395, 1396, 1397, 1398, 1399, 1400, 1401, 1402, 1403, 1404, 1405, 1406, 1407, 1408, 1409, 1410, 1411, 1412, 1413, 1414, 1415, 1416, 1417, 1418, 1419, 1420, 1421, 1422, 1423, 1424, 1425, 1426, 1427, 1428, 1429, 1430, 1431, 1432, 1433, 1434, 1435, 1436, 1437, 1438, 1439, 1440, 1441, 1442, 1443, 1444, 1445, 1446, 1447, 1448],
    "icon": "paw"
  },
  "order": ["Base", "CAHe1", "CAHe2", "CAHe3", "CAHe4", "CAHe5", "CAHe6", "science", "reject", "Canadian", "c-ladies", "NSFH", "c-stupid", "c-sodomydog"]
}
